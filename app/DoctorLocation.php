<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorLocation extends Model
{
    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }
    public function location()
    {
        return $this->belongsTo('App\Location');
    }
    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }
}
