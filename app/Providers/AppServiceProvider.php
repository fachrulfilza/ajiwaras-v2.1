<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\AdminWhatsappNumber;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $wa_config = AdminWhatsappNumber::where('name', 'admin')->first();
        if($wa_config != null){
            View::share('nomor_wa', $wa_config->phone);
            View::share('text_wa', $wa_config->text);
        }else{
            View::share('nomor_wa', '#');
            View::share('text_wa', '#');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
