<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VisiMisi;
use App\About;
use App\Location;
class HomeController extends Controller
{
    public function index()
    {   
        $visi_misi = VisiMisi::all();
        $about = About::first();
        $locations = Location::all();
        
        return view('welcome', compact('visi_misi', 'about', 'locations'));
    }
}
