<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\DoctorLocation;

class DoctorController extends Controller
{
    public function show($id)
    {
        $doctor = Doctor::findOrFail($id);
        $doctor_locations = DoctorLocation::where('doctor_id', $id)->with('doctor.schedule_lines')->get();
        return view('doctor-profile', compact('doctor', 'doctor_locations'));
    }
}
