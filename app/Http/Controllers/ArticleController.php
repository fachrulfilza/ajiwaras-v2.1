<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();

        return redirect()->route('home');
        //return view('articles', compact('articles'));
    }

    public function show($id)
    {
        $article = Article::find($id);

        return redirect()->route('home');
        //return view('article', compact('article'));
    }
}
