<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;

class LocationController extends Controller
{
    public function getAllLocation(Request $request)
    {
        $locations = Location::where('is_clinic', 1)->get();
        if(isset($request->location_id)){
            $locations = Location::where([
                ['id', '!=', $request->location_id],
                'is_clinic' => 1
            ])->get();
        }

        return response()->json($locations);
    }
}
