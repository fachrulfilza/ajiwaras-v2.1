<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    public function index($slug)
    {
        $service = Service::where('slug', $slug)->firstOrFail();
        
        return view('service', compact('service'));
    }
}
