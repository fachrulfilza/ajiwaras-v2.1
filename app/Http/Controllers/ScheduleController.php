<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\DoctorLocation;

class ScheduleController extends Controller
{
    public function index()
    {
        $locations = Location::where('is_clinic', 1)->get();
   
        return view('schedules', compact('locations'));
    }

    public function getScheduleByLoc($location_id, Request $request)
    {
        $schedules = DoctorLocation::where('location_id', $location_id)
                    ->with('doctor', 'doctor.schedule_lines', 'location', 'doctor.specialist')->get();
        if(isset($request->q)){
            $q = $request->q;
            $schedules = DoctorLocation::where('location_id', $location_id)
                    ->whereHas('doctor', function($query) use ($q){
                        $query->where('name', 'like', '%'.$q.'%');
                    })
                    ->with('doctor', 'doctor.schedule_lines', 'location', 'doctor.specialist')
                    ->get();
            if(count($schedules) == 0){
                $schedules = DoctorLocation::where('location_id', $location_id)
                    ->whereHas('doctor.specialist', function($query) use ($q){
                        $query->where('name', 'like', '%'.$q.'%');
                    })
                    ->with('doctor', 'doctor.schedule_lines', 'location', 'doctor.specialist')
                    ->get();
            }
        }
        return response()->json($schedules);
    }
}
