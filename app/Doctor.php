<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    public function schedule_lines()
    {
        return $this->hasMany('App\ScheduleLine', 'doctor_id');
    }
    public function specialist()
    {
        return $this->belongsTo('App\DoctorSpecialist', 'specialist_id');
    }
    public function doctor_locations()
    {
        return $this->hasMany('App\DoctorLocation', 'doctor_id');
    }
}
