-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 05, 2019 at 04:05 PM
-- Server version: 10.2.27-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajia6989_ajiwaras`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `description`, `img`, `created_at`, `updated_at`) VALUES
(1, 'AJIWARAS Medical Centre didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.', NULL, '2019-06-04 12:41:45', '2019-10-02 05:46:10');

-- --------------------------------------------------------

--
-- Table structure for table `admin_whatsapp_numbers`
--

CREATE TABLE `admin_whatsapp_numbers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_whatsapp_numbers`
--

INSERT INTO `admin_whatsapp_numbers` (`id`, `name`, `phone`, `text`, `created_at`, `updated_at`) VALUES
(1, 'admin', '081', 'Halo Admin', '2019-07-08 07:10:15', '2019-10-02 05:26:11');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Artikel 1', 'uploads/1/2019-07/alexander_popov_627477_unsplash.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2019-07-14 12:07:03', NULL),
(2, 'Artikel 2', 'uploads/1/2019-07/alexander_popov_757362_unsplash.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2019-07-14 12:07:11', NULL),
(3, 'Artikel 3', 'uploads/1/2019-07/joyful_woman_holding_cellphone_gesturing_thumbup_against_yellow_surface_23_2148188743.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2019-07-14 12:07:21', NULL),
(4, 'Artikel 4', 'uploads/1/2019-07/trendy_clean_promo2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2019-07-14 12:07:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responses` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_attachments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-06-04 12:05:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/users/edit-save/1', 'Update data Super Admin at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2019-06/user.png</td></tr><tr><td>email</td><td>admin@crudbooster.com</td><td>root@ajiwaras.com</td></tr><tr><td>password</td><td>$2y$10$rbrMYr6.tYplO07PeZVfpeTr9B0.VJgJ1lsjwsovG6fxXD1IsTfZm</td><td>$2y$10$sOuk9kv84jPFF84do9rHAeBJ/PPBe0dMImgDM3SAOIdNDk6V17Rl.</td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2019-06-04 12:06:08', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/services/add-save', 'Add New Data Apotek at Layanan', '', 1, '2019-06-04 12:17:15', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/services/add-save', 'Add New Data Laboratorium at Layanan', '', 1, '2019-06-04 12:18:41', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/services/add-save', 'Add New Data Klinik at Layanan', '', 1, '2019-06-04 12:19:06', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/services/add-save', 'Add New Data Fisioterapi at Layanan', '', 1, '2019-06-04 12:19:21', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/visi_misi/add-save', 'Add New Data Visi at Visi & Misi', '', 1, '2019-06-04 12:33:40', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/visi_misi/add-save', 'Add New Data Misi at Visi & Misi', '', 1, '2019-06-04 12:33:55', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/visi_misi/edit-save/2', 'Update data Visi at Visi & Misi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Misi</td><td>Visi</td></tr></tbody></table>', 1, '2019-06-04 12:34:00', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/visi_misi/edit-save/1', 'Update data Misi at Visi & Misi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Visi</td><td>Misi</td></tr></tbody></table>', 1, '2019-06-04 12:34:06', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/abouts/add-save', 'Add New Data  at Tentang Kami', '', 1, '2019-06-04 12:41:45', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/john-schnobrich-520023-unsplash.jpg</td></tr></tbody></table>', 1, '2019-06-04 12:42:25', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/john-schnobrich-520023-unsplash.jpg</td></tr></tbody></table>', 1, '2019-06-04 12:46:05', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/service-page-bg.jpg</td></tr></tbody></table>', 1, '2019-06-04 12:46:17', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-06-05 22:36:24', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/5', 'Update data Spesialis Dokter at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Spesialis</td><td>Spesialis Dokter</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>5</td><td></td></tr></tbody></table>', 1, '2019-06-05 22:41:46', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_specialists/add-save', 'Add New Data Gigi at Spesialis Dokter', '', 1, '2019-06-05 22:45:21', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_specialists/add-save', 'Add New Data Jantung at Spesialis Dokter', '', 1, '2019-06-05 22:45:27', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_specialists/add-save', 'Add New Data Sarag at Spesialis Dokter', '', 1, '2019-06-05 22:45:29', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_specialists/edit-save/3', 'Update data Saraf at Spesialis Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Sarag</td><td>Saraf</td></tr></tbody></table>', 1, '2019-06-05 22:45:37', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/locations/add-save', 'Add New Data Cilandak at Lokasi', '', 1, '2019-06-05 22:55:02', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctors/add-save', 'Add New Data Dr. Esther Gunawan at Daftar Dokter', '', 1, '2019-06-05 22:56:14', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-05 22:56:22', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/8', 'Update data Jadwal Praktek at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Jadwal Praktet</td><td>Jadwal Praktek</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 1, '2019-06-05 23:11:24', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 1, '2019-06-05 23:43:03', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/1', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>schedule_type</td><td></td><td>Berdasarkan Kedatangan</td></tr></tbody></table>', 1, '2019-06-05 23:43:08', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 1, '2019-06-05 23:43:49', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/2', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>schedule_type</td><td></td><td>Berdasarkan Jam</td></tr></tbody></table>', 1, '2019-06-05 23:43:52', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-06-06 08:56:30', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/locations/add-save', 'Add New Data Cipete at Lokasi', '', 1, '2019-06-06 08:57:51', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/locations/add-save', 'Add New Data Fatmawati at Lokasi', '', 1, '2019-06-06 08:58:52', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/locations/add-save', 'Add New Data Cipedak at Lokasi', '', 1, '2019-06-06 08:59:43', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:30:53', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/delete/1', 'Delete data 1 at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:30:57', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:41:13', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:41:24', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/delete/3', 'Delete data 3 at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:42:28', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/delete/2', 'Delete data 2 at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:42:30', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:42:48', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:42:56', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctors/add-save', 'Add New Data Dr. John at Daftar Dokter', '', 1, '2019-06-06 09:50:12', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-06 09:50:21', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/2', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>schedule_type</td><td>Berdasarkan Jam</td><td></td></tr><tr><td>location_id</td><td>0</td><td>1</td></tr></tbody></table>', 1, '2019-06-06 11:22:33', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/1', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>schedule_type</td><td>Berdasarkan Kedatangan</td><td></td></tr><tr><td>location_id</td><td>0</td><td>1</td></tr></tbody></table>', 1, '2019-06-06 11:22:38', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/2', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-06-06 11:23:22', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-06-07 12:19:39', NULL),
(46, '36.84.241.104', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 36.84.241.104', '', 1, '2019-06-11 07:41:34', NULL),
(47, '36.84.241.104', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Arip at Daftar Dokter', '', 1, '2019-06-11 07:44:00', NULL),
(48, '36.84.241.104', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-11 07:44:14', NULL),
(49, '118.136.113.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 118.136.113.173', '', 1, '2019-06-20 08:12:37', NULL),
(50, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 139.194.174.88', '', 1, '2019-06-21 05:41:54', NULL),
(51, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/users/add-save', 'Add New Data Pingkan at Users Management', '', 1, '2019-06-21 05:43:51', NULL),
(52, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data tes at Daftar Dokter', '', 1, '2019-06-21 05:45:06', NULL),
(53, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/4', 'Delete data tes at Daftar Dokter', '', 1, '2019-06-21 05:45:10', NULL),
(54, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 1, '2019-06-21 05:46:48', NULL),
(55, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 1, '2019-06-21 05:47:54', NULL),
(56, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/delete/3', 'Delete data 3 at Jadwal Praktek', '', 1, '2019-06-21 05:48:22', NULL),
(57, '114.124.245.64', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 114.124.245.64', '', 1, '2019-07-02 04:39:44', NULL),
(58, '114.124.245.64', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/users/edit-save/2', 'Update data Pingkan at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$IR4G2eXV7fQRMhjvRikVn.l/CFZikm5pGV259LUl3hz3FBm7BuXpW</td><td>$2y$10$wMN42btwzz.baveShNYQH.yC0UdJ4v.Uq4YkPSi3PqwczGok0U5TK</td></tr><tr><td>status</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-02 04:40:13', NULL),
(59, '114.124.245.64', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/logout', 'root@ajiwaras.com logout', '', 1, '2019-07-02 04:43:24', NULL),
(60, '114.124.245.64', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'pmpinkrose7@gmail.com login with IP Address 114.124.245.64', '', 2, '2019-07-02 04:43:29', NULL),
(61, '114.124.245.64', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/logout', 'pmpinkrose7@gmail.com logout', '', 2, '2019-07-02 04:43:34', NULL),
(62, '115.178.206.211', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'pmpinkrose7@gmail.com login with IP Address 115.178.206.211', '', 2, '2019-07-02 04:53:02', NULL),
(63, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data ESTHER JULIAWATI SUBIJANTO, DRG. at Daftar Dokter', '', 2, '2019-07-02 05:22:27', NULL),
(64, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Umum at Spesialis Dokter', '', 2, '2019-07-02 05:23:21', NULL),
(65, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Kulit & Kelamin at Spesialis Dokter', '', 2, '2019-07-02 05:23:29', NULL),
(66, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Mata at Spesialis Dokter', '', 2, '2019-07-02 05:23:34', NULL),
(67, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Penyakit Dalam at Spesialis Dokter', '', 2, '2019-07-02 05:23:41', NULL),
(68, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Saraf at Spesialis Dokter', '', 2, '2019-07-02 05:23:46', NULL),
(69, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data THT at Spesialis Dokter', '', 2, '2019-07-02 05:23:56', NULL),
(70, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Physicology at Spesialis Dokter', '', 2, '2019-07-02 05:24:16', NULL),
(71, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Kebidanan & Penyakit Kandungan at Spesialis Dokter', '', 2, '2019-07-02 05:25:08', NULL),
(72, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/3', 'Delete data Arip at Daftar Dokter', '', 2, '2019-07-02 05:25:21', NULL),
(73, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/2', 'Delete data Dr. John at Daftar Dokter', '', 2, '2019-07-02 05:25:25', NULL),
(74, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/1', 'Delete data Dr. Esther Gunawan at Daftar Dokter', '', 2, '2019-07-02 05:25:29', NULL),
(75, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Amiyati Arfiyani, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:26:37', NULL),
(76, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/5', 'Update data Esther Juliawati Subijanto, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>ESTHER JULIAWATI SUBIJANTO, DRG.</td><td>Esther Juliawati Subijanto, Drg.</td></tr></tbody></table>', 2, '2019-07-02 05:27:02', NULL),
(77, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Asih Lubis, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:27:31', NULL),
(78, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Dedy Soehermawan, SP. OG at Daftar Dokter', '', 2, '2019-07-02 05:28:00', NULL),
(79, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Elly Moedijatmini, SP. KK at Daftar Dokter', '', 2, '2019-07-02 05:28:23', NULL),
(80, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Endang Setyawati, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:28:48', NULL),
(81, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Endang Setyawati, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:28:49', NULL),
(82, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/9', 'Update data Elly Moedijatmini, SP. KK, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Elly Moedijatmini, SP. KK</td><td>Elly Moedijatmini, SP. KK, Dr.</td></tr></tbody></table>', 2, '2019-07-02 05:29:11', NULL),
(83, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/8', 'Update data Dedy Soehermawan, SP. OG, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Dedy Soehermawan, SP. OG</td><td>Dedy Soehermawan, SP. OG, Dr.</td></tr></tbody></table>', 2, '2019-07-02 05:29:28', NULL),
(84, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Evy Novita P. Tarigan, SP, PD., Dr. at Daftar Dokter', '', 2, '2019-07-02 05:30:18', NULL),
(85, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/delete/3', 'Delete data Saraf at Spesialis Dokter', '', 2, '2019-07-02 05:31:43', NULL),
(86, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/9', 'Delete data 9 at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:32:03', NULL),
(87, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/9', 'Delete data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:32:03', NULL),
(88, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/7', 'Delete data 7 at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:32:13', NULL),
(89, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/6', 'Delete data 6 at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:32:17', NULL),
(90, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/5', 'Delete data 5 at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:32:21', NULL),
(91, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/4', 'Delete data 4 at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:32:24', NULL),
(92, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/locations/add-save', 'Add New Data Karang Tengah at Lokasi', '', 2, '2019-07-02 05:35:54', NULL),
(93, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Fitriani Nasution, SP, S, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:36:58', NULL),
(94, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Gunawan Dibjojuwono, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:37:18', NULL),
(95, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data H. Afif Abbas, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:37:41', NULL),
(96, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Hasim Rusli, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:37:56', NULL),
(97, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Indriani Pudjiastuti, SP., M, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:38:28', NULL),
(98, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Ismeila Murtie, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:38:47', NULL),
(99, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Kukuh Suryo, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:39:05', NULL),
(100, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Novita Eka Sukma Putri, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:40:13', NULL),
(101, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Nusyirwan Basri, SP. M, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:40:35', NULL),
(102, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Rien N. Hadinoto, SP, KK, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:41:04', NULL),
(103, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Srie Enggar, SP. A., Dr. at Daftar Dokter', '', 2, '2019-07-02 05:41:37', NULL),
(104, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_specialists/add-save', 'Add New Data Anak at Spesialis Dokter', '', 2, '2019-07-02 05:41:46', NULL),
(105, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/23', 'Update data Srie Enggar, SP. A., Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>specialist_id</td><td>6</td><td>12</td></tr></tbody></table>', 2, '2019-07-02 05:41:59', NULL),
(106, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Sylvia Retnosari, SP. A, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:44:07', NULL),
(107, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Sylvia Retnosari, SP. A, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:44:07', NULL),
(108, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Tri Damiyatno, SP. THT, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:44:39', NULL),
(109, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Yuli Susilowati, SPI, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:46:09', NULL),
(110, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Yeti Retno Lestari, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:46:36', NULL),
(111, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Anna Fitri Fawzia, Drg at Daftar Dokter', '', 2, '2019-07-02 05:47:10', NULL),
(112, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Atria Mya Kelani, SP.KG, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:47:49', NULL),
(113, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Fransiska Monika Lago, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:48:24', NULL),
(114, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Hari Sumitro, SP, BM, Drg at Daftar Dokter', '', 2, '2019-07-02 05:49:02', NULL),
(115, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Herna Emmy Himawati, Drg at Daftar Dokter', '', 2, '2019-07-02 05:49:22', NULL),
(116, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Inggrid Amelia, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:49:42', NULL),
(117, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Lily, SP. Perio, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:50:03', NULL),
(118, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Linda Verniati Tjokrosuwirjo, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:50:34', NULL),
(119, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Merlies Meoko at Daftar Dokter', '', 2, '2019-07-02 05:51:22', NULL),
(120, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Merlies Meoko at Daftar Dokter', '', 2, '2019-07-02 05:51:22', NULL),
(121, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Pande Putu Lily Trisna Dewi, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:51:44', NULL),
(122, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Ruth Marina, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:52:00', NULL),
(123, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Ruth Marina, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:52:00', NULL),
(124, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Sariesendy, SP. Ortho, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:52:24', NULL),
(125, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Sariesendy, SP. Ortho, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:52:24', NULL),
(126, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Titus Dermawan, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:52:39', NULL),
(127, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Titus Dermawan, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:52:39', NULL),
(128, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Fifky Pantow, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:53:04', NULL),
(129, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Fifky Pantow, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:53:04', NULL),
(130, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Kristin Indriati, SP, KK, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:53:27', NULL),
(131, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Siskawati HW. SP, KK, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:53:59', NULL),
(132, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Siskawati HW. SP, KK, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:53:59', NULL),
(133, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/add-save', 'Add New Data Medwin Setia, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:54:15', NULL),
(134, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/47', 'Delete data Fifky Pantow, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:54:49', NULL),
(135, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/37', 'Delete data Merlies Meoko at Daftar Dokter', '', 2, '2019-07-02 05:55:13', NULL),
(136, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/41', 'Delete data Ruth Marina, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:55:23', NULL),
(137, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/43', 'Delete data Sariesendy, SP. Ortho, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:55:40', NULL),
(138, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/50', 'Delete data Siskawati HW. SP, KK, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:55:48', NULL),
(139, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/25', 'Delete data Sylvia Retnosari, SP. A, Dr. at Daftar Dokter', '', 2, '2019-07-02 05:55:56', NULL),
(140, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/45', 'Delete data Titus Dermawan, Drg. at Daftar Dokter', '', 2, '2019-07-02 05:56:19', NULL),
(141, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:57:00', NULL),
(142, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/48', 'Update data Kristin Indriati Hariningsih, SP, KK, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Kristin Indriati, SP, KK, Dr.</td><td>Kristin Indriati Hariningsih, SP, KK, Dr.</td></tr></tbody></table>', 2, '2019-07-02 05:57:55', NULL),
(143, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/49', 'Update data Siskawati H.W. SP, KK, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Siskawati HW. SP, KK, Dr.</td><td>Siskawati H.W. SP, KK, Dr.</td></tr></tbody></table>', 2, '2019-07-02 05:58:21', NULL),
(144, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/delete/8', 'Delete data 8 at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:58:48', NULL),
(145, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:59:02', NULL),
(146, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:59:15', NULL),
(147, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:59:26', NULL),
(148, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:59:38', NULL),
(149, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 05:59:49', NULL),
(150, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:00:04', NULL),
(151, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:00:14', NULL),
(152, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:00:24', NULL),
(153, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:00:33', NULL),
(154, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:00:42', NULL),
(155, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/edit-save/38', 'Update data Merlies Meoko, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Merlies Meoko</td><td>Merlies Meoko, Drg.</td></tr></tbody></table>', 2, '2019-07-02 06:01:14', NULL),
(156, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:01:37', NULL),
(157, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:01:52', NULL),
(158, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctor_locations/add-save', 'Add New Data  at Lokasi Praktek Dokter', '', 2, '2019-07-02 06:01:59', NULL),
(159, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'pmpinkrose7@gmail.com login with IP Address 139.194.174.88', '', 2, '2019-07-03 02:26:15', NULL),
(160, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/delete/2', 'Delete data 2 at Jadwal Praktek', '', 2, '2019-07-03 02:27:42', NULL),
(161, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/delete/1', 'Delete data 1 at Jadwal Praktek', '', 2, '2019-07-03 02:27:45', NULL),
(162, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:29:08', NULL),
(163, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:29:36', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(164, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:30:29', NULL),
(165, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:31:07', NULL),
(166, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:31:41', NULL),
(167, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:32:11', NULL),
(168, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:33:22', NULL),
(169, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:33:55', NULL),
(170, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:34:44', NULL),
(171, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:35:21', NULL),
(172, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:36:19', NULL),
(173, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:37:14', NULL),
(174, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:38:02', NULL),
(175, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:38:33', NULL),
(176, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:39:08', NULL),
(177, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:39:49', NULL),
(178, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:39:49', NULL),
(179, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:40:19', NULL),
(180, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:41:18', NULL),
(181, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:41:58', NULL),
(182, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:43:11', NULL),
(183, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.46 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 2, '2019-07-03 02:43:44', NULL),
(184, '114.124.245.69', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 114.124.245.69', '', 1, '2019-07-03 04:27:13', NULL),
(185, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-07-03 05:16:26', NULL),
(186, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/5', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-07-03 05:42:19', NULL),
(187, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/edit-save/7', 'Update data  at Jadwal Praktek', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>schedule_type</td><td>Berdasarkan Kedatangan</td><td>Berdasarkan Perjanjian</td></tr></tbody></table>', 1, '2019-07-03 05:43:14', NULL),
(188, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/doctors/add-save', 'Add New Data tes 1 at Daftar Dokter', '', 1, '2019-07-03 05:44:19', NULL),
(189, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 1, '2019-07-03 05:59:22', NULL),
(190, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/delete/26', 'Delete data 26 at Jadwal Praktek', '', 1, '2019-07-03 05:59:28', NULL),
(191, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/add-save', 'Add New Data  at Jadwal Praktek', '', 1, '2019-07-03 06:00:33', NULL),
(192, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/schedule_lines/delete/27', 'Delete data 27 at Jadwal Praktek', '', 1, '2019-07-03 06:00:37', NULL),
(193, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/5', 'Update data Karang Tengah at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 06:29:37', NULL),
(194, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 06:32:03', NULL),
(195, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/5', 'Update data Karang Tengah at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>is_clinic</td><td>0</td><td>1</td></tr></tbody></table>', 1, '2019-07-03 06:32:08', NULL),
(196, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 06:32:12', NULL),
(197, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 06:32:16', NULL),
(198, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 06:32:19', NULL),
(199, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 06:32:22', NULL),
(200, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/1', 'Update data Cilandak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>is_pharmacy</td><td>0</td><td>1</td></tr><tr><td>is_clinic</td><td>0</td><td>1</td></tr></tbody></table>', 1, '2019-07-03 06:32:27', NULL),
(201, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-07-03 09:24:58', NULL),
(202, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td>(021) 788 83556</td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 09:26:30', NULL),
(203, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td>(021) 751 9306</td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 09:27:04', NULL),
(204, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td>(021) 751 3850</td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td>(021) 7590 6022</td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 09:27:48', NULL),
(205, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/1', 'Update data Cilandak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td>(021) 7883 6308</td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td>(021) 7883 6307</td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 09:28:33', NULL),
(206, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td>(021) 751 3850</td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 09:28:57', NULL),
(207, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/5', 'Update data Karang Tengah at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td>021-75909473</td><td>(021) 75909473</td></tr><tr><td>pharmacy_number</td><td>021-7655433</td><td>(021) 7655433</td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-03 09:29:13', NULL),
(208, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/login', 'pmpinkrose7@gmail.com login with IP Address 127.0.0.1', '', 2, '2019-07-08 07:08:52', NULL),
(209, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/users/edit-save/1', 'Update data Super Admin at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$sOuk9kv84jPFF84do9rHAeBJ/PPBe0dMImgDM3SAOIdNDk6V17Rl.</td><td>$2y$10$kxDdqtn1lgwy9UldTraeNePWNtVKj.Jz0b7X.7WBdM059hn12Imdq</td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 2, '2019-07-08 07:09:06', NULL),
(210, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/logout', 'pmpinkrose7@gmail.com logout', '', 2, '2019-07-08 07:09:14', NULL),
(211, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-07-08 07:09:20', NULL),
(212, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/admin_whatsapp_numbers/add-save', 'Add New Data admin at Nomor Whatsapp', '', 1, '2019-07-08 07:10:15', NULL),
(213, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/5', 'Update data Karang Tengah at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td>Senin - Jumat 07:00 - 22:00</td></tr><tr><td>pharmacy_schedule</td><td></td><td>Senin - Jumat 08:00 - 22:00</td></tr></tbody></table>', 1, '2019-07-08 07:17:06', NULL),
(214, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td></td></tr><tr><td>pharmacy_schedule</td><td></td><td>Setiap Hari 07:00 - 22:00</td></tr></tbody></table>', 1, '2019-07-08 07:17:40', NULL),
(215, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td>Senin - Jumat 07:00 - 22:00</td></tr><tr><td>pharmacy_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:17:48', NULL),
(216, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td>Senin - Jumat 07:00 - 22:00</td></tr><tr><td>pharmacy_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:17:54', NULL),
(217, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>pharmacy_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:17:58', NULL),
(218, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>pharmacy_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:18:02', NULL),
(219, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/1', 'Update data Cilandak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td>Senin - Jumat 07:00 - 22:00</td></tr><tr><td>pharmacy_schedule</td><td></td><td>Setiap Hari 07:00 - 22:00</td></tr></tbody></table>', 1, '2019-07-08 07:18:08', NULL),
(220, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/1', 'Update data Cilandak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>pharmacy_schedule</td><td>Setiap Hari 07:00 - 22:00</td><td>Senin - Jumat 07:00 - 22:00</td></tr></tbody></table>', 1, '2019-07-08 07:18:28', NULL),
(221, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>pharmacy_schedule</td><td></td><td>Senin - Jumat 07:00 - 22:00</td></tr></tbody></table>', 1, '2019-07-08 07:18:33', NULL),
(222, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>pharmacy_schedule</td><td></td><td>Senin - Jumat 07:00 - 22:00</td></tr></tbody></table>', 1, '2019-07-08 07:18:40', NULL),
(223, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:18:48', NULL),
(224, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:18:52', NULL),
(225, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/5', 'Update data Karang Tengah at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:18:56', NULL),
(226, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:21:47', NULL),
(227, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:21:55', NULL),
(228, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:22:14', NULL),
(229, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:22:35', NULL),
(230, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:22:47', NULL),
(231, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:22:54', NULL),
(232, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td>.</td><td></td></tr></tbody></table>', 1, '2019-07-08 07:23:00', NULL),
(233, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td>.</td><td></td></tr></tbody></table>', 1, '2019-07-08 07:24:54', NULL),
(234, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/3', 'Update data Fatmawati at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>fax</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td>.</td><td>-</td></tr></tbody></table>', 1, '2019-07-08 07:25:05', NULL),
(235, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/4', 'Update data Cipedak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>clinic_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr><tr><td>clinic_schedule</td><td></td><td>-</td></tr></tbody></table>', 1, '2019-07-08 07:25:23', NULL),
(236, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/2', 'Update data Cipete at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:25:32', NULL),
(237, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/locations/edit-save/1', 'Update data Cilandak at Lokasi', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td></td><td></td></tr><tr><td>pharmacy_number</td><td></td><td></td></tr><tr><td>whatsapp</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-08 07:25:38', NULL),
(238, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/login', 'root@ajiwaras.com login with IP Address 127.0.0.1', '', 1, '2019-07-14 12:04:05', NULL),
(239, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/articles/add-save', 'Add New Data Artikel 1 at Artikel', '', 1, '2019-07-14 12:07:03', NULL),
(240, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/articles/add-save', 'Add New Data Artikel 2 at Artikel', '', 1, '2019-07-14 12:07:11', NULL),
(241, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/articles/add-save', 'Add New Data Artikel 3 at Artikel', '', 1, '2019-07-14 12:07:21', NULL),
(242, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://localhost:8000/admin/articles/add-save', 'Add New Data Artikel 4 at Artikel', '', 1, '2019-07-14 12:07:29', NULL),
(243, '114.124.175.134', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/doctors/delete/52', 'Delete data tes 1 at Daftar Dokter', '', 1, '2019-07-26 17:27:20', NULL),
(244, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'pmpinkrose7@gmail.com login with IP Address 139.194.174.88', '', 2, '2019-08-01 06:59:31', NULL),
(245, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt expedita dolorum laboriosam, placeat incidunt debitis sapiente labore autem, doloribus minima minus laudantium nulla voluptas sint totam, voluptatibus veniam quia rem? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit, voluptas. Iusto hic illum quasi voluptates qui officia modi, esse perspiciatis obcaecati vero placeat tempore, accusantium at cumque assumenda rem? Voluptates!</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengankomitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.</td></tr><tr><td>img</td><td>uploads/1/service-page-bg.jpg</td><td></td></tr></tbody></table>', 2, '2019-08-01 07:18:09', NULL),
(246, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengankomitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.</td></tr></tbody></table>', 2, '2019-08-01 07:19:31', NULL),
(247, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 07:19:31', NULL),
(248, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt expedita dolorum laboriosam, placeat incidunt debitis sapiente labore autem, doloribus minima minus laudantium nulla voluptas sint totam, voluptatibus veniam quia rem? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit, voluptas. Iusto hic illum quasi voluptates qui officia modi, esse perspiciatis obcaecati vero placeat tempore, accusantium at cumque assumenda rem? Voluptates!</td><td>Laboratorium Ajiwaras dilengkapi dengan peralatan laboratorium yang lengkap dan tenaga analis yang profesional serta dibawah pengawasan yang ketat oleh dokter spesialis patologi klinik yang handal dan berpengalaman dibidangnya.\r\n\r\nKami pastikan hasil laboratorium yang diberikan akurat dan tepat, sehingga dapat membantu dokter dalam memberikan diagnosa dan mengambil langkah yang tepat dalam penanganan, pengobatan dan pemulihan kesehatan anda.</td></tr><tr><td>img</td><td>uploads/1/service-page-bg.jpg</td><td></td></tr></tbody></table>', 2, '2019-08-01 07:22:34', NULL),
(249, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt expedita dolorum laboriosam, placeat incidunt debitis sapiente labore autem, doloribus minima minus laudantium nulla voluptas sint totam, voluptatibus veniam quia rem? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit, voluptas. Iusto hic illum quasi voluptates qui officia modi, esse perspiciatis obcaecati vero placeat tempore, accusantium at cumque assumenda rem? Voluptates!</td><td>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelumdisetujui. Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. Keramahan seluruh karyawan apotek dalam melayani pasien,. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. Realisasi dari proses perbaikan ditunjangoleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.</td></tr><tr><td>img</td><td>uploads/1/service-page-bg.jpg</td><td></td></tr></tbody></table>', 2, '2019-08-01 07:30:42', NULL),
(250, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 07:30:42', NULL),
(251, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 07:30:42', NULL),
(252, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Indra Maulana\r\n\r\nDokter Gigi</td></tr></tbody></table>', 2, '2019-08-01 07:34:31', NULL),
(253, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 07:34:31', NULL),
(254, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Indra Maulana\r\n\r\nDokter Gigi</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Amiyati\r\nDr. Endang\r\nDr. Ismeila Murtie\r\nDr. Yety Retno L.\r\nDr. Kukuh Suryo\r\nDr. Fifky Pantow\r\n\r\nDokter Gigi :\r\nDrg. Esther Juliawati\r\nDrg. Ingrid Amelia\r\nDrg. Titus Dermawan\r\nDrg. Herna Emmy H\r\nDrs. Lily, Sp. Perio\r\nDrg. Hari Sumitro\r\nDrg. Linda V. Sp. Ortho\r\nDrg. Sariesendi, Sp. Ortho\r\nDrg. Francis Monika Lago\r\nDrg. Atria, Sp. KG\r\nDrg. Merlies Meoko\r\n\r\nDokter Spesialis Syaraf :\r\nDr. Fitriani Nasution, Sp.S\r\n\r\nDokter Spesialis Anak :\r\nDr. Srie Enggar E, Sp. A\r\nDr. Sylvia Retnosari, Sp.A\r\n\r\nDokter Spesialis Kandungan :\r\nDr. Dedy Soehermawan, Sp.OG\r\n\r\nDokter Spesialis THT :\r\nDr. TriDamiyatno, Sp. THT\r\n\r\nDokter Spesialis Penyakit Dalan \r\nDr. Evy Novita, Sp. PD\r\n\r\nDokter Spesialis Kulit dan Kelamin :\r\nDr. Rien N. Hadinoto, Sp. KK\r\nDr. Elly Moedijatmini, Sp. KK\r\nDr. Kristin Indrati Hariningsih, Sp. KK\r\nDr. Siskawati H.W, Sp. KK\r\n\r\nDokter Spesialis Mata :\r\nDr. Indriani, Sp. M\r\nDr. Nusyirwan, Sp. M\r\nDr. Novita, Sp.M\r\n\r\nPsikolog\r\nDrs. Yuli Susilowati, PSI</td></tr></tbody></table>', 2, '2019-08-01 07:54:54', NULL),
(255, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 07:54:54', NULL),
(256, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 07:54:54', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(257, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Amiyati\r\nDr. Endang\r\nDr. Ismeila Murtie\r\nDr. Yety Retno L.\r\nDr. Kukuh Suryo\r\nDr. Fifky Pantow\r\n\r\nDokter Gigi :\r\nDrg. Esther Juliawati\r\nDrg. Ingrid Amelia\r\nDrg. Titus Dermawan\r\nDrg. Herna Emmy H\r\nDrs. Lily, Sp. Perio\r\nDrg. Hari Sumitro\r\nDrg. Linda V. Sp. Ortho\r\nDrg. Sariesendi, Sp. Ortho\r\nDrg. Francis Monika Lago\r\nDrg. Atria, Sp. KG\r\nDrg. Merlies Meoko\r\n\r\nDokter Spesialis Syaraf :\r\nDr. Fitriani Nasution, Sp.S\r\n\r\nDokter Spesialis Anak :\r\nDr. Srie Enggar E, Sp. A\r\nDr. Sylvia Retnosari, Sp.A\r\n\r\nDokter Spesialis Kandungan :\r\nDr. Dedy Soehermawan, Sp.OG\r\n\r\nDokter Spesialis THT :\r\nDr. TriDamiyatno, Sp. THT\r\n\r\nDokter Spesialis Penyakit Dalan \r\nDr. Evy Novita, Sp. PD\r\n\r\nDokter Spesialis Kulit dan Kelamin :\r\nDr. Rien N. Hadinoto, Sp. KK\r\nDr. Elly Moedijatmini, Sp. KK\r\nDr. Kristin Indrati Hariningsih, Sp. KK\r\nDr. Siskawati H.W, Sp. KK\r\n\r\nDokter Spesialis Mata :\r\nDr. Indriani, Sp. M\r\nDr. Nusyirwan, Sp. M\r\nDr. Novita, Sp.M\r\n\r\nPsikolog\r\nDrs. Yuli Susilowati, PSI</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Ajiwaras Medical Center maju dan berkembang untuk memberikan layanan terbaik kepada masyarakat dengan mengikuti standar dan teknologi terkini. \r\n\r\nKlinik Ajiwaras disediakan bagi anda yang membutuhkan pelayanan terpercaya dan di tangani oleh dokterspesialis di bidangny.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Amiyati\r\nDr. Endang\r\nDr. Ismeila Murtie\r\nDr. Yety Retno L.\r\nDr. Kukuh Suryo\r\nDr. Fifky Pantow\r\n\r\nDokter Gigi :\r\nDrg. Esther Juliawati\r\nDrg. Ingrid Amelia\r\nDrg. Titus Dermawan\r\nDrg. Herna Emmy H\r\nDrs. Lily, Sp. Perio\r\nDrg. Hari Sumitro\r\nDrg. Linda V. Sp. Ortho\r\nDrg. Sariesendi, Sp. Ortho\r\nDrg. Francis Monika Lago\r\nDrg. Atria, Sp. KG\r\nDrg. Merlies Meoko\r\n\r\nDokter Spesialis Syaraf :\r\nDr. Fitriani Nasution, Sp.S\r\n\r\nDokter Spesialis Anak :\r\nDr. Srie Enggar E, Sp. A\r\nDr. Sylvia Retnosari, Sp.A\r\n\r\nDokter Spesialis Kandungan :\r\nDr. Dedy Soehermawan, Sp.OG\r\n\r\nDokter Spesialis THT :\r\nDr. TriDamiyatno, Sp. THT\r\n\r\nDokter Spesialis Penyakit Dalan \r\nDr. Evy Novita, Sp. PD\r\n\r\nDokter Spesialis Kulit dan Kelamin :\r\nDr. Rien N. Hadinoto, Sp. KK\r\nDr. Elly Moedijatmini, Sp. KK\r\nDr. Kristin Indrati Hariningsih, Sp. KK\r\nDr. Siskawati H.W, Sp. KK\r\n\r\nDokter Spesialis Mata :\r\nDr. Indriani, Sp. M\r\nDr. Nusyirwan, Sp. M\r\nDr. Novita, Sp.M\r\n\r\nPsikolog\r\nDrs. Yuli Susilowati, PSI</td></tr></tbody></table>', 2, '2019-08-01 07:59:40', NULL),
(258, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Laboratorium Ajiwaras dilengkapi dengan peralatan laboratorium yang lengkap dan tenaga analis yang profesional serta dibawah pengawasan yang ketat oleh dokter spesialis patologi klinik yang handal dan berpengalaman dibidangnya.\r\n\r\nKami pastikan hasil laboratorium yang diberikan akurat dan tepat, sehingga dapat membantu dokter dalam memberikan diagnosa dan mengambil langkah yang tepat dalam penanganan, pengobatan dan pemulihan kesehatan anda.</td><td>Laboratorium Ajiwaras dilengkapi dengan peralatan laboratorium yang lengkap dan tenaga analis yang profesional serta dibawah pengawasan yang ketat oleh dokter spesialis patologi klinik yang handal dan berpengalaman dibidangnya.\r\n\r\nKami pastikan hasil laboratorium yang diberikan akurat dan tepat, sehingga dapat membantu dokter dalam memberikan diagnosa dan mengambil langkah yang tepat dalam penanganan, pengobatan dan pemulihan kesehatan anda.\r\n\r\nFasilitas Pemeriksaan Laboratorium :\r\nHematologi\r\nKoagulasi\r\nUrin\r\nTinja\r\nKimia Klinik\r\nSerologi\r\nRematologi\r\nPetanda Tumor\r\nTiroid\r\nHormon Reproduksi\r\nImunologi\r\nPetanda Tulang</td></tr></tbody></table>', 2, '2019-08-01 08:01:50', NULL),
(259, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/4', 'Update data Fisioterapi at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt expedita dolorum laboriosam, placeat incidunt debitis sapiente labore autem, doloribus minima minus laudantium nulla voluptas sint totam, voluptatibus veniam quia rem? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit, voluptas. Iusto hic illum quasi voluptates qui officia modi, esse perspiciatis obcaecati vero placeat tempore, accusantium at cumque assumenda rem? Voluptates!</td><td>Fasilitas Layanan Fisioterapi :\r\nElektroterapi\r\nMekanoterapi\r\nAktinoterapi\r\nChest fisioterapi</td></tr><tr><td>img</td><td>uploads/1/service-page-bg.jpg</td><td></td></tr></tbody></table>', 2, '2019-08-01 08:02:42', NULL),
(260, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelumdisetujui. Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. Keramahan seluruh karyawan apotek dalam melayani pasien,. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. Realisasi dari proses perbaikan ditunjangoleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.</td><td>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelumdisetujui. Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. Keramahan seluruh karyawan apotek dalam melayani pasien,. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. Realisasi dari proses perbaikan ditunjangoleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.\r\n\r\nFilosofi mutu layanan Apotek :\r\n1. Memberikan pelayanan terbaik dengan ketulusan hati untuk semua lapisan masyarakat\r\n2. Kualitas obat yang terjamin secara mutu dengan harga terjangkau\r\n3. Menjaga keaslian dan kelengkapan obat\r\n4. Memberikan layanan pesan antaruntuk daerah sekitar Apotek Ajiwaras</td></tr></tbody></table>', 2, '2019-08-01 08:05:16', NULL),
(261, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-01 08:05:16', NULL),
(262, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td></tr><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-08-01 08:31:12', NULL),
(263, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td></tr><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-08-01 08:31:12', NULL),
(264, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td></tr><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-08-01 08:33:37', NULL),
(265, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-08-01 08:33:37', NULL),
(266, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td><td>AJIWARAS Medical Centre didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td></tr><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-08-01 08:34:13', NULL),
(267, '118.136.113.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 118.136.113.173', '', 1, '2019-08-01 20:04:11', NULL),
(268, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'pmpinkrose7@gmail.com login with IP Address 139.194.174.88', '', 2, '2019-08-02 01:50:21', NULL),
(269, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>AJIWARAS Medical Centre didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.\r\n\r\nKantor Pusat & Cabang AJIWARAS\r\nKantor Pusat :\r\nAjiwaras Medical Center Cilandak\r\nJl. Cilandak KKO Raya No.45, Cilandak Jakarta Selatan\r\nKlinik (021) 7883 6308, 7883 57003\r\nApotek (021) 7806 089, 7883 6307\r\nFax. (021) 7883 6307\r\nWebsite : www.ajiwaras.com\r\nEmail : ajiwaras@ajiwaras.com\r\n\r\nCabang AJIWARAS :\r\nAjiwaras Medical Centre Karang Tengah\r\nJl. Karang Tengah Raya No.60, Lebak Bulus, Jakarta Selatan\r\nKlinik (021) 7590 9473\r\nApotek (021) 765 5433, 7074 5008\r\nFax. (021) 7590 9454\r\n\r\nAjiawaras Medical Centre Cipete\r\nJl. Cipete Raya No.76 C, Cipete, Jakarta Selatan\r\nTelp (021) 751 3580, 7590 6022\r\nFax. (021) 7590 6022\r\n\r\nAjiawaras Medical Centre Fatmawati\r\nJl. RS. Fatmawati No.46 C, Fatmawati, Jakarta Selatan\r\nTelp. (021) 751 9306, 759 2016\r\n\r\nAjiwaras Medical Centre Cipedak\r\nJl. M Kahfi I No.45, Cipedak, Jakarta Selatan\r\nTelp. (021) 788 83556\r\nFax (021) 788 83556</td><td>AJIWARAS Medical Centre didirikan sejak tahun 1999 oleh Dr. Gunawan D.J dan Dr. Esther Gunawan. Sejak saat itu kami terus menempatkan inovasi dan pertumbuhan layanan kesehatan. Kami yakin akan dapat memberikan kualitas layanan kesehatan yang baik kepada masyarakat.\r\n\r\nSarana dan Prasarana AJIWARAS\r\nRuang tunggu\r\nAjiwaras Medical Center telah dirancang guna kenyamanan pasien. Desain interiornya bersifat minimalis, hangat dan terdapat disemua ruang layanan kesehatan.\r\n\r\nKantin kecil\r\nKantin kecil Ajiwaras dengan desain yang minimalis yang tentu saja tidak melupakan sis higienisnya juga disediakan bagi anda yang ingin berbelanja makanan ringan (snack) ataupun minuman ringan (softdrink)\r\n\r\nPenyelesaian pembiayaan\r\nUntuk Anda yang memiliki asuransi kesehatan pribadi, akun Anda akan langsung dimasukkan ke dalamnya. Pada saat pembiayaan, Anda akan mendapatkan kwitansi dari Ajiwaras Medical Center, yang tentunya disesuaikan dengan tingkat dan jenis asuransi Anda. Biaya dokter, prosedur diagnosa, dan operasi akan dirinci secara terpisah. Staf kami akan memastikan bahwa Anda mengerti seluruh proses ini.\r\n\r\nSementara untuk pasien yang tidak memiliki asuransi kesehatan akan diminta untuk menyelesaikan pembayaran pada saat akhir kunjungan.\r\n\r\nAntar Jemput :\r\nJasa layanan antar jemput disediakan bagi pasien yang ingin menggunakan layanan Ajiwaras Medical Center seperti pengambilan sampling untuk analisa laboratorium hingga layanan apotek.\r\n\r\nKemanan dan Kamera CCTV\r\nLayanan petugas keamanan yang ramah dan informatif kami tempatkan di semua cabang kami, guna memastikan keamanan yang lebih baik, kami juga menempatkan CCTV dibeberapa titik.\r\n\r\nParkir :\r\nDengan lapangan parkir yang luas dan aman, memudahkan Anda mendapatkan parkir di sekitar lingkungan Ajiwaras Medical Center.\r\n\r\nMANAJEMEN AJIWARAS :\r\nDirektur Utama : dr. Gunawan DJ\r\nManager Operasional : dr. Esther Gunawan\r\nPenanggung Jawab Klinik : dr. Gunawan DJ\r\nPenanggung Jawa Laboratorium : dr. H.M Afif Abbas\r\nKonsultan Laboratorium :\r\ndr. Agus Kosasih, Sp. PK. MARS\r\n\r\nRekan kami tahu bahwa mereka dapat mengandalkan bentuk kerjasama tim. Kami terbuka dan jujur dalam berkomunikasi, merangkul umpan balik yang konstruktif, mengakui prestasi dan mendapatkan rasa hormat satu sama lain serta menerimanya sebagai sesuatu yang wajar adanya.</td></tr><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-08-02 01:51:17', NULL),
(270, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Ajiwaras Medical Center maju dan berkembang untuk memberikan layanan terbaik kepada masyarakat dengan mengikuti standar dan teknologi terkini. \r\n\r\nKlinik Ajiwaras disediakan bagi anda yang membutuhkan pelayanan terpercaya dan di tangani oleh dokterspesialis di bidangny.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Amiyati\r\nDr. Endang\r\nDr. Ismeila Murtie\r\nDr. Yety Retno L.\r\nDr. Kukuh Suryo\r\nDr. Fifky Pantow\r\n\r\nDokter Gigi :\r\nDrg. Esther Juliawati\r\nDrg. Ingrid Amelia\r\nDrg. Titus Dermawan\r\nDrg. Herna Emmy H\r\nDrs. Lily, Sp. Perio\r\nDrg. Hari Sumitro\r\nDrg. Linda V. Sp. Ortho\r\nDrg. Sariesendi, Sp. Ortho\r\nDrg. Francis Monika Lago\r\nDrg. Atria, Sp. KG\r\nDrg. Merlies Meoko\r\n\r\nDokter Spesialis Syaraf :\r\nDr. Fitriani Nasution, Sp.S\r\n\r\nDokter Spesialis Anak :\r\nDr. Srie Enggar E, Sp. A\r\nDr. Sylvia Retnosari, Sp.A\r\n\r\nDokter Spesialis Kandungan :\r\nDr. Dedy Soehermawan, Sp.OG\r\n\r\nDokter Spesialis THT :\r\nDr. TriDamiyatno, Sp. THT\r\n\r\nDokter Spesialis Penyakit Dalan \r\nDr. Evy Novita, Sp. PD\r\n\r\nDokter Spesialis Kulit dan Kelamin :\r\nDr. Rien N. Hadinoto, Sp. KK\r\nDr. Elly Moedijatmini, Sp. KK\r\nDr. Kristin Indrati Hariningsih, Sp. KK\r\nDr. Siskawati H.W, Sp. KK\r\n\r\nDokter Spesialis Mata :\r\nDr. Indriani, Sp. M\r\nDr. Nusyirwan, Sp. M\r\nDr. Novita, Sp.M\r\n\r\nPsikolog\r\nDrs. Yuli Susilowati, PSI</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Ajiwaras Medical Center maju dan berkembang untuk memberikan layanan terbaik kepada masyarakat dengan mengikuti standar dan teknologi terkini. \r\n\r\nKlinik Ajiwaras disediakan bagi anda yang membutuhkan pelayanan terpercaya dan di tangani oleh dokter spesialis dibidangnya.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Amiyati\r\nDr. Endang\r\nDr. Ismeila Murtie\r\nDr. Yety Retno L.\r\nDr. Kukuh Suryo\r\nDr. Fifky Pantow\r\n\r\nDokter Gigi :\r\nDrg. Esther Juliawati\r\nDrg. Ingrid Amelia\r\nDrg. Titus Dermawan\r\nDrg. Herna Emmy H\r\nDrs. Lily, Sp. Perio\r\nDrg. Hari Sumitro\r\nDrg. Linda V. Sp. Ortho\r\nDrg. Sariesendi, Sp. Ortho\r\nDrg. Francis Monika Lago\r\nDrg. Atria, Sp. KG\r\nDrg. Merlies Meoko\r\n\r\nDokter Spesialis Syaraf :\r\nDr. Fitriani Nasution, Sp.S\r\n\r\nDokter Spesialis Anak :\r\nDr. Srie Enggar E, Sp. A\r\nDr. Sylvia Retnosari, Sp.A\r\n\r\nDokter Spesialis Kandungan :\r\nDr. Dedy Soehermawan, Sp.OG\r\n\r\nDokter Spesialis THT :\r\nDr. TriDamiyatno, Sp. THT\r\n\r\nDokter Spesialis Penyakit Dalan \r\nDr. Evy Novita, Sp. PD\r\n\r\nDokter Spesialis Kulit dan Kelamin :\r\nDr. Rien N. Hadinoto, Sp. KK\r\nDr. Elly Moedijatmini, Sp. KK\r\nDr. Kristin Indrati Hariningsih, Sp. KK\r\nDr. Siskawati H.W, Sp. KK\r\n\r\nDokter Spesialis Mata :\r\nDr. Indriani, Sp. M\r\nDr. Nusyirwan, Sp. M\r\nDr. Novita, Sp.M\r\n\r\nPsikolog\r\nDrs. Yuli Susilowati, PSI</td></tr></tbody></table>', 2, '2019-08-02 02:32:08', NULL),
(271, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-02 02:33:21', NULL),
(272, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelumdisetujui. Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. Keramahan seluruh karyawan apotek dalam melayani pasien,. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. Realisasi dari proses perbaikan ditunjangoleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.\r\n\r\nFilosofi mutu layanan Apotek :\r\n1. Memberikan pelayanan terbaik dengan ketulusan hati untuk semua lapisan masyarakat\r\n2. Kualitas obat yang terjamin secara mutu dengan harga terjangkau\r\n3. Menjaga keaslian dan kelengkapan obat\r\n4. Memberikan layanan pesan antaruntuk daerah sekitar Apotek Ajiwaras</td><td>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelum disetujui. Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. Keramahan seluruh karyawan apotek dalam melayani pasien. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. Realisasi dari proses perbaikan ditunjang oleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.\r\n\r\nFilosofi mutu layanan Apotek :\r\n1. Memberikan pelayanan terbaik dengan ketulusan hati untuk semua lapisan masyarakat\r\n2. Kualitas obat yang terjamin secara mutu dengan harga terjangkau\r\n3. Menjaga keaslian dan kelengkapan obat\r\n4. Memberikan layanan pesan antaruntuk daerah sekitar Apotek Ajiwaras</td></tr></tbody></table>', 2, '2019-08-02 02:34:48', NULL),
(273, '139.194.174.88', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-08-02 02:34:48', NULL),
(274, '182.0.164.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/login', 'root@ajiwaras.com login with IP Address 182.0.164.226', '', 1, '2019-08-02 23:43:19', NULL),
(275, '182.0.164.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/4', 'Update data Fisioterapi at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-08-02 23:44:31', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(276, '182.0.164.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.\r\n\r\nKlinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n\r\nLayanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n\r\nKlinik Ajiwaras Medical Center maju dan berkembang untuk memberikan layanan terbaik kepada masyarakat dengan mengikuti standar dan teknologi terkini. \r\n\r\nKlinik Ajiwaras disediakan bagi anda yang membutuhkan pelayanan terpercaya dan di tangani oleh dokter spesialis dibidangnya.\r\n\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\nDr. Gunawan D.J\r\nDr. H.M Afif Abbas\r\nDr. Asih Lubis\r\nDr. Hasyim Rusli\r\nDr. Amiyati\r\nDr. Endang\r\nDr. Ismeila Murtie\r\nDr. Yety Retno L.\r\nDr. Kukuh Suryo\r\nDr. Fifky Pantow\r\n\r\nDokter Gigi :\r\nDrg. Esther Juliawati\r\nDrg. Ingrid Amelia\r\nDrg. Titus Dermawan\r\nDrg. Herna Emmy H\r\nDrs. Lily, Sp. Perio\r\nDrg. Hari Sumitro\r\nDrg. Linda V. Sp. Ortho\r\nDrg. Sariesendi, Sp. Ortho\r\nDrg. Francis Monika Lago\r\nDrg. Atria, Sp. KG\r\nDrg. Merlies Meoko\r\n\r\nDokter Spesialis Syaraf :\r\nDr. Fitriani Nasution, Sp.S\r\n\r\nDokter Spesialis Anak :\r\nDr. Srie Enggar E, Sp. A\r\nDr. Sylvia Retnosari, Sp.A\r\n\r\nDokter Spesialis Kandungan :\r\nDr. Dedy Soehermawan, Sp.OG\r\n\r\nDokter Spesialis THT :\r\nDr. TriDamiyatno, Sp. THT\r\n\r\nDokter Spesialis Penyakit Dalan \r\nDr. Evy Novita, Sp. PD\r\n\r\nDokter Spesialis Kulit dan Kelamin :\r\nDr. Rien N. Hadinoto, Sp. KK\r\nDr. Elly Moedijatmini, Sp. KK\r\nDr. Kristin Indrati Hariningsih, Sp. KK\r\nDr. Siskawati H.W, Sp. KK\r\n\r\nDokter Spesialis Mata :\r\nDr. Indriani, Sp. M\r\nDr. Nusyirwan, Sp. M\r\nDr. Novita, Sp.M\r\n\r\nPsikolog\r\nDrs. Yuli Susilowati, PSI</td><td><p>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.</p><p><br></p><p>Klinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n</p><p><br></p><p>Layanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n</p><p><br></p><p>\r\nKlinik Ajiwaras Medical Center maju dan berkembang untuk memberikan layanan terbaik kepada masyarakat dengan mengikuti standar dan teknologi terkini. \r\n\r\nKlinik Ajiwaras disediakan bagi anda yang membutuhkan pelayanan terpercaya dan di tangani oleh dokter spesialis dibidangnya.\r\n</p><p>\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\n</p><p>Dr. Gunawan D.J</p><p>Dr. H.M Afif Abbas\r\n</p><p>Dr. Asih Lubis\r\n</p><p>Dr. Hasyim Rusli</p><p>\r\nDr. Amiyati\r\n</p><p>Dr. Endang\r\n</p><p>Dr. Ismeila Murtie\r\n</p><p>Dr. Yety Retno L.\r\n</p><p>Dr. Kukuh Suryo</p><p>\r\nDr. Fifky Pantow</p><p>\r\n\r\nDokter Gigi :\r\n</p><p>Drg. Esther Juliawati</p><p>\r\nDrg. Ingrid Amelia\r\n</p><p>Drg. Titus Dermawan\r\n</p><p>Drg. Herna Emmy H\r\n</p><p>Drs. Lily, Sp. Perio</p><p>\r\nDrg. Hari Sumitro\r\n</p><p>Drg. Linda V. Sp. Ortho\r\n</p><p>Drg. Sariesendi, Sp. Ortho</p><p>\r\nDrg. Francis Monika Lago\r\n</p><p>Drg. Atria, Sp. KG\r\n<br>Drg. Merlies Meoko\r\n</p><p>\r\nDokter Spesialis Syaraf :\r\n</p><p>Dr. Fitriani Nasution, Sp.S\r\n</p><p>\r\nDokter Spesialis Anak :\r\n</p><p>Dr. Srie Enggar E, Sp. A\r\n</p><p>Dr. Sylvia Retnosari, Sp.A\r\n</p><p>\r\nDokter Spesialis Kandungan :\r\n</p><p>Dr. Dedy Soehermawan, Sp.OG\r\n</p><p>\r\nDokter Spesialis THT :</p><p>\r\nDr. TriDamiyatno, Sp. THT</p><p>\r\n\r\nDokter Spesialis Penyakit Dalam :</p><p> \r\nDr. Evy Novita, Sp. PD\r\n</p><p>\r\nDokter Spesialis Kulit dan Kelamin :\r\n</p><p>Dr. Rien N. Hadinoto, Sp. KK\r\n</p><p>Dr. Elly Moedijatmini, Sp. KK\r\n</p><p>Dr. Kristin Indrati Hariningsih, Sp. KK\r\n</p><p>Dr. Siskawati H.W, Sp. KK\r\n</p><p>\r\nDokter Spesialis Mata :\r\n</p><p>Dr. Indriani, Sp. M\r\n</p><p>Dr. Nusyirwan, Sp. M\r\n</p><p>Dr. Novita, Sp.M\r\n</p><p>\r\nPsikolog :</p><p>Drs. Yuli Susilowati, PSI</p></td></tr></tbody></table>', 1, '2019-08-02 23:48:13', NULL),
(277, '182.0.164.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'http://dev-ajiwaras.muhzaa.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-08/img2.jpg</td></tr></tbody></table>', 1, '2019-08-02 23:50:03', NULL),
(278, '180.244.68.17', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'http://ajiwaras.com/admin/login', 'root@ajiwaras.com login with IP Address 180.244.68.17', '', 1, '2019-08-10 00:59:21', NULL),
(279, '180.244.68.17', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'http://ajiwaras.com/admin/login', 'root@ajiwaras.com login with IP Address 180.244.68.17', '', 1, '2019-08-10 00:59:23', NULL),
(280, '180.244.68.17', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'http://ajiwaras.com/admin/services/delete-image', 'Delete the image of Klinik at Layanan', '', 1, '2019-08-10 01:06:52', NULL),
(281, '180.244.68.17', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'http://ajiwaras.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-08/alexander_popov_627477_unsplash.jpg</td></tr></tbody></table>', 1, '2019-08-10 01:07:19', NULL),
(282, '182.0.228.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'http://ajiwaras.com/admin/login', 'root@ajiwaras.com login with IP Address 182.0.228.144', '', 1, '2019-08-10 05:37:21', NULL),
(283, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/login', 'root@ajiwaras.com login with IP Address 118.136.113.148', '', 1, '2019-09-12 04:24:34', NULL),
(284, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/5', 'Update data Esther Juliawati Subijanto, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:25:27', NULL),
(285, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/14', 'Update data Gunawan Dibjojuwono, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:25:40', NULL),
(286, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/15', 'Update data H. Afif Abbas, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:25:52', NULL),
(287, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/46', 'Update data Fifky Pantow, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:26:08', NULL),
(288, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/9', 'Update data Elly Moedijatmini, SP. KK, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:26:27', NULL),
(289, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/36', 'Update data Linda Verniati Tjokrosuwirjo, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:26:39', NULL),
(290, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/32', 'Update data Hari Sumitro, SP, BM, Drg at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:26:58', NULL),
(291, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/33', 'Update data Herna Emmy Himawati, Drg at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:27:21', NULL),
(292, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/8', 'Update data Dedy Soehermawan, SP. OG, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:27:32', NULL),
(293, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/19', 'Update data Kukuh Suryo, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:27:43', NULL),
(294, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/42', 'Update data Sariesendy, SP. Ortho, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:27:55', NULL),
(295, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/44', 'Update data Titus Dermawan, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:28:06', NULL),
(296, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/21', 'Update data Nusyirwan Basri, SP. M, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:28:19', NULL),
(297, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/13', 'Update data Fitriani Nasution, SP, S, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:28:33', NULL),
(298, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/31', 'Update data Fransiska Monika Lago, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:28:46', NULL),
(299, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/delete/10', 'Delete data Endang Setyawati, Dr. at Daftar Dokter', '', 1, '2019-09-12 04:29:01', NULL),
(300, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/11', 'Update data Endang Setyawati, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:29:09', NULL),
(301, '118.136.113.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/40', 'Update data Ruth Marina, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-09-12 04:29:22', NULL),
(302, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/login', 'pmpinkrose7@gmail.com login with IP Address 139.194.82.208', '', 2, '2019-10-02 04:46:28', NULL),
(303, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/51', 'Update data Medwin Setia, Drg. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-10-02 04:55:02', NULL),
(304, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/15', 'Update data H. Afif Abbas, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>graduates</td><td>Sekolah Tinggi Kedokteran</td><td>Sekolah Tinggi Kedokteran \"YARSI\"</td></tr></tbody></table>', 2, '2019-10-02 05:00:37', NULL),
(305, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/doctors/edit-save/11', 'Update data Endang Setyawati, Dr. at Daftar Dokter', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-10-02 05:02:16', NULL),
(306, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/services/edit-save/4', 'Update data Fisioterapi at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Fasilitas Layanan Fisioterapi :\r\nElektroterapi\r\nMekanoterapi\r\nAktinoterapi\r\nChest fisioterapi</td><td><p><span style=\"color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif; font-size: 16px;\">Fisioterapi bertujuan untuk mengembalikan fungsi tubuh setelah terkena penyakit atau cedera. Jika tubuh menderita penyakit atau cedera permanen, maka fisioterapi dapat diprioritaskan untuk mengurangi dampaknya.</span><br></p><p>Fasilitas Layanan Fisioterapi : Elektroterapi Mekanoterapi Aktinoterapi Chest fisioterapi</p><div><br></div><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p></td></tr></tbody></table>', 2, '2019-10-02 05:19:14', NULL),
(307, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/admin_whatsapp_numbers/edit-save/1', 'Update data admin at Nomor Whatsapp', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>phone</td><td>081234567890</td><td>081</td></tr></tbody></table>', 2, '2019-10-02 05:26:11', NULL),
(308, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-10-02 05:27:03', NULL),
(309, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-10-02 05:27:54', NULL),
(310, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelum disetujui. Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. Keramahan seluruh karyawan apotek dalam melayani pasien. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. Realisasi dari proses perbaikan ditunjang oleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.\r\n\r\nFilosofi mutu layanan Apotek :\r\n1. Memberikan pelayanan terbaik dengan ketulusan hati untuk semua lapisan masyarakat\r\n2. Kualitas obat yang terjamin secara mutu dengan harga terjangkau\r\n3. Menjaga keaslian dan kelengkapan obat\r\n4. Memberikan layanan pesan antaruntuk daerah sekitar Apotek Ajiwaras</td><td><p>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelum disetujui. </p><p>Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. </p><p>Keramahan seluruh karyawan apotek dalam melayani pasien. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n</p><p>\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. </p><p>Realisasi dari proses perbaikan ditunjang oleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.\r\n</p><p>\r\nFilosofi mutu layanan Apotek :\r\n</p><p>1. Memberikan pelayanan terbaik dengan ketulusan hati untuk semua lapisan masyarakat</p><p>2. Kualitas obat yang terjamin secara mutu dengan harga terjangkau\r\n</p><p>3. Menjaga keaslian dan kelengkapan obat\r\n</p><p>4. Memberikan layanan pesan antar untuk daerah sekitar Apotek Ajiwaras</p></td></tr></tbody></table>', 2, '2019-10-02 05:34:09', NULL),
(311, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>description</td><td>Laboratorium Ajiwaras dilengkapi dengan peralatan laboratorium yang lengkap dan tenaga analis yang profesional serta dibawah pengawasan yang ketat oleh dokter spesialis patologi klinik yang handal dan berpengalaman dibidangnya.\r\n\r\nKami pastikan hasil laboratorium yang diberikan akurat dan tepat, sehingga dapat membantu dokter dalam memberikan diagnosa dan mengambil langkah yang tepat dalam penanganan, pengobatan dan pemulihan kesehatan anda.\r\n\r\nFasilitas Pemeriksaan Laboratorium :\r\nHematologi\r\nKoagulasi\r\nUrin\r\nTinja\r\nKimia Klinik\r\nSerologi\r\nRematologi\r\nPetanda Tumor\r\nTiroid\r\nHormon Reproduksi\r\nImunologi\r\nPetanda Tulang</td><td><p>Laboratorium Ajiwaras dilengkapi dengan peralatan laboratorium yang lengkap dan tenaga analis yang profesional serta dibawah pengawasan yang ketat oleh dokter spesialis patologi klinik yang handal dan berpengalaman dibidangnya.\r\n</p><p>\r\nKami pastikan hasil laboratorium yang diberikan akurat dan tepat, sehingga dapat membantu dokter dalam memberikan diagnosa dan mengambil langkah yang tepat dalam penanganan, pengobatan dan pemulihan kesehatan anda.\r\n</p><p>\r\nFasilitas Pemeriksaan Laboratorium :\r\n</p><p>- Hematologi\r\nKoagulasi\r\nUrin\r\nTinja\r\nKimia Klinik\r\n</p><p>- Serologi\r\n</p><p>- Rematologi\r\nPetanda Tumor\r\n</p><p>- Tiroid\r\n</p><p>- Hormon Reproduksi\r\n</p><p>- Imunologi\r\nPetanda Tulang</p></td></tr></tbody></table>', 2, '2019-10-02 05:45:53', NULL),
(312, '139.194.82.208', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.34 Safari/537.36', 'http://ajiwaras.com/admin/abouts/edit-save/1', 'Update data  at Tentang Kami', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td></td></tr></tbody></table>', 2, '2019-10-02 05:46:10', NULL),
(313, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/login', 'root@ajiwaras.com login with IP Address 180.252.226.186', '', 1, '2019-10-02 07:20:20', NULL),
(314, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/4', 'Update data Fisioterapi at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/toa_heftiba_hblf2nvp_yc_unsplash.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:25:51', NULL),
(315, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/delete-image', 'Delete the image of Klinik at Layanan', '', 1, '2019-10-02 07:26:03', NULL),
(316, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/3', 'Update data Klinik at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/martha_dominguez_de_gouveia_nmym7fxpoke_unsplash.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:26:14', NULL),
(317, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/tbel_abuseridze_ebw1nlfdzfw_unsplash_compressor.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:33:34', NULL),
(318, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/delete-image', 'Delete the image of Apotek at Layanan', '', 1, '2019-10-02 07:35:17', NULL),
(319, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/tbel_abuseridze_ebw1nlfdzfw_unsplash_compressor.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:35:32', NULL),
(320, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/lucas_vasques_9vnacvx2748_unsplash.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:39:27', NULL),
(321, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/delete-image', 'Delete the image of Laboratorium at Layanan', '', 1, '2019-10-02 07:40:14', NULL),
(322, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/lucas_vasques_9vnacvx2748_unsplash_compressor.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:40:26', NULL),
(323, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/delete-image', 'Delete the image of Apotek at Layanan', '', 1, '2019-10-02 07:40:41', NULL),
(324, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/delete-image', 'Delete the image of Laboratorium at Layanan', '', 1, '2019-10-02 07:41:27', NULL),
(325, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/2', 'Update data Laboratorium at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/0010376b_800.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:41:34', NULL),
(326, '180.252.226.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'https://ajiwaras.com/admin/services/edit-save/1', 'Update data Apotek at Layanan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>img</td><td></td><td>uploads/1/2019-10/5.jpg</td></tr></tbody></table>', 1, '2019-10-02 07:43:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_dashboard` tinyint(1) NOT NULL DEFAULT 0,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Layanan', 'Route', 'AdminServicesControllerGetIndex', NULL, 'fa fa-list-alt', 0, 1, 0, 1, 6, '2019-06-04 12:06:47', NULL),
(2, 'Visi & Misi', 'Route', 'AdminVisiMisiControllerGetIndex', NULL, 'fa fa-star-o', 0, 1, 0, 1, 5, '2019-06-04 12:32:59', NULL),
(3, 'Tentang Kami', 'Route', 'AdminAboutsControllerGetIndex', NULL, 'fa fa-home', 0, 1, 0, 1, 7, '2019-06-04 12:40:00', NULL),
(4, 'Daftar Dokter', 'Route', 'AdminDoctorsControllerGetIndex', NULL, 'fa fa-users', 0, 1, 0, 1, 1, '2019-06-05 22:37:16', NULL),
(5, 'Spesialis Dokter', 'Route', 'AdminDoctorSpecialistsControllerGetIndex', 'normal', 'fa fa-heart', 0, 1, 0, 1, 2, '2019-06-05 22:40:54', '2019-06-05 22:41:46'),
(6, 'Lokasi', 'Route', 'AdminLocationsControllerGetIndex', NULL, 'fa fa-map-pin', 0, 1, 0, 1, 8, '2019-06-05 22:42:27', NULL),
(7, 'Lokasi Praktek Dokter', 'Route', 'AdminDoctorLocationsControllerGetIndex', NULL, 'fa fa-map-marker', 0, 1, 0, 1, 3, '2019-06-05 22:44:14', NULL),
(8, 'Jadwal Praktek', 'Route', 'AdminScheduleLinesControllerGetIndex', 'normal', 'fa fa-calendar', 0, 1, 0, 1, 4, '2019-06-05 23:09:50', '2019-06-05 23:11:24'),
(9, 'Nomor Whatsapp', 'Route', 'AdminAdminWhatsappNumbersControllerGetIndex', NULL, 'fa fa-phone', 0, 1, 0, 1, 9, '2019-07-08 07:09:40', NULL),
(10, 'Artikel', 'Route', 'AdminArticlesControllerGetIndex', NULL, 'fa fa-pencil', 0, 1, 0, 1, 10, '2019-07-14 12:06:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(6, 5, 1),
(7, 6, 1),
(8, 7, 1),
(10, 8, 1),
(11, 9, 1),
(12, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2019-06-04 12:05:34', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2019-06-04 12:05:34', NULL, NULL),
(12, 'Layanan', 'fa fa-list-alt', 'services', 'services', 'AdminServicesController', 0, 0, '2019-06-04 12:06:47', NULL, NULL),
(13, 'Visi & Misi', 'fa fa-star-o', 'visi_misi', 'visi_misi', 'AdminVisiMisiController', 0, 0, '2019-06-04 12:32:59', NULL, NULL),
(14, 'Tentang Kami', 'fa fa-home', 'abouts', 'abouts', 'AdminAboutsController', 0, 0, '2019-06-04 12:40:00', NULL, NULL),
(15, 'Daftar Dokter', 'fa fa-users', 'doctors', 'doctors', 'AdminDoctorsController', 0, 0, '2019-06-05 22:37:15', NULL, NULL),
(16, 'Spesialis Dokter', 'fa fa-heart', 'doctor_specialists', 'doctor_specialists', 'AdminDoctorSpecialistsController', 0, 0, '2019-06-05 22:40:54', NULL, NULL),
(17, 'Lokasi', 'fa fa-map-pin', 'locations', 'locations', 'AdminLocationsController', 0, 0, '2019-06-05 22:42:27', NULL, NULL),
(18, 'Lokasi Praktek Dokter', 'fa fa-map-marker', 'doctor_locations', 'doctor_locations', 'AdminDoctorLocationsController', 0, 0, '2019-06-05 22:44:14', NULL, NULL),
(19, 'Jadwal Praktek', 'fa fa-calendar', 'schedule_lines', 'schedule_lines', 'AdminScheduleLinesController', 0, 0, '2019-06-05 23:09:50', NULL, NULL),
(20, 'Nomor Whatsapp', 'fa fa-phone', 'admin_whatsapp_numbers', 'admin_whatsapp_numbers', 'AdminAdminWhatsappNumbersController', 0, 0, '2019-07-08 07:09:40', NULL, NULL),
(21, 'Artikel', 'fa fa-pencil', 'articles', 'articles', 'AdminArticlesController', 0, 0, '2019-07-14 12:06:08', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2019-06-04 12:05:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2019-06-04 12:05:34', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2019-06-04 12:05:34', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2019-06-04 12:05:34', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2019-06-04 12:05:34', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2019-06-04 12:05:34', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2019-06-04 12:05:34', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2019-06-04 12:05:34', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2019-06-04 12:05:34', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2019-06-04 12:05:34', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2019-06-04 12:05:34', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2019-06-04 12:05:34', NULL),
(12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(20, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(21, 1, 1, 1, 1, 1, 1, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2019-06-04 12:05:34', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2019-06-04 12:05:34', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2019-06-04 12:05:34', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2019-06-04 12:05:34', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'CRUDBooster', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2019-06-04 12:05:34', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', '', 'upload_image', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', '', 'upload_image', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2019-06-04 12:05:34', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2019-06-04 12:05:34', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', 'uploads/1/2019-06/user.png', 'root@ajiwaras.com', '$2y$10$kxDdqtn1lgwy9UldTraeNePWNtVKj.Jz0b7X.7WBdM059hn12Imdq', 1, '2019-06-04 12:05:34', '2019-07-08 07:09:06', 'Active'),
(2, 'Pingkan', 'uploads/1/2019-06/logo_ajiwaras.jpg', 'pmpinkrose7@gmail.com', '$2y$10$wMN42btwzz.baveShNYQH.yC0UdJ4v.Uq4YkPSi3PqwczGok0U5TK', 1, '2019-06-21 05:43:51', '2019-07-02 04:40:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(10) UNSIGNED NOT NULL,
  `specialist_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `graduates` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sip_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `specialist_id`, `name`, `graduates`, `description`, `image`, `sip_number`, `created_at`, `updated_at`) VALUES
(5, 1, 'Esther Juliawati Subijanto, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:22:27', '2019-09-12 04:25:27'),
(6, 4, 'Amiyati Arfiyani, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:26:37', NULL),
(7, 4, 'Asih Lubis, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:27:31', NULL),
(8, 11, 'Dedy Soehermawan, SP. OG, Dr.', 'Universitas Diponegoro', NULL, NULL, NULL, '2019-07-02 05:28:00', '2019-09-12 04:27:32'),
(9, 5, 'Elly Moedijatmini, SP. KK, Dr.', 'Universitas gajah Mada', NULL, NULL, NULL, '2019-07-02 05:28:23', '2019-09-12 04:26:27'),
(11, 4, 'Endang Setyawati, Dr.', 'Universitas Katolik Indonesia Atmajaya', NULL, NULL, NULL, '2019-07-02 05:28:49', '2019-10-02 05:02:16'),
(12, 7, 'Evy Novita P. Tarigan, SP, PD., Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:30:18', NULL),
(13, 8, 'Fitriani Nasution, SP, S, Dr.', 'Universitas Padjajaran', NULL, NULL, NULL, '2019-07-02 05:36:58', '2019-09-12 04:28:33'),
(14, 4, 'Gunawan Dibjojuwono, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:37:18', '2019-09-12 04:25:40'),
(15, 4, 'H. Afif Abbas, Dr.', 'Sekolah Tinggi Kedokteran \"YARSI\"', NULL, NULL, NULL, '2019-07-02 05:37:41', '2019-10-02 05:00:37'),
(16, 4, 'Hasim Rusli, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:37:56', NULL),
(17, 6, 'Indriani Pudjiastuti, SP., M, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:38:28', NULL),
(18, 4, 'Ismeila Murtie, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:38:47', NULL),
(19, 4, 'Kukuh Suryo, Dr.', 'Universitas Indonesia', NULL, NULL, NULL, '2019-07-02 05:39:05', '2019-09-12 04:27:43'),
(20, 6, 'Novita Eka Sukma Putri, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:40:13', NULL),
(21, 6, 'Nusyirwan Basri, SP. M, Dr.', 'Universitas Indonesia', NULL, NULL, NULL, '2019-07-02 05:40:35', '2019-09-12 04:28:19'),
(22, 5, 'Rien N. Hadinoto, SP, KK, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:41:04', NULL),
(23, 12, 'Srie Enggar, SP. A., Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:41:37', '2019-07-02 05:41:59'),
(24, 12, 'Sylvia Retnosari, SP. A, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:44:07', NULL),
(26, 9, 'Tri Damiyatno, SP. THT, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:44:39', NULL),
(27, 10, 'Yuli Susilowati, SPI, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:46:09', NULL),
(28, 4, 'Yeti Retno Lestari, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:46:36', NULL),
(29, 1, 'Anna Fitri Fawzia, Drg', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:47:10', NULL),
(30, 1, 'Atria Mya Kelani, SP.KG, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:47:49', NULL),
(31, 1, 'Fransiska Monika Lago, Drg.', 'Universitas Indonesia', NULL, NULL, NULL, '2019-07-02 05:48:24', '2019-09-12 04:28:46'),
(32, 1, 'Hari Sumitro, SP, BM, Drg', 'Kedokteran Gigi TNI AL RE Martadinata', NULL, NULL, NULL, '2019-07-02 05:49:02', '2019-09-12 04:26:58'),
(33, 1, 'Herna Emmy Himawati, Drg', 'Universitas Trisakti', NULL, NULL, NULL, '2019-07-02 05:49:22', '2019-09-12 04:27:21'),
(34, 1, 'Inggrid Amelia, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:49:42', NULL),
(35, 1, 'Lily, SP. Perio, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:50:03', NULL),
(36, 1, 'Linda Verniati Tjokrosuwirjo, Drg.', 'Universitas Airlangga', NULL, NULL, NULL, '2019-07-02 05:50:34', '2019-09-12 04:26:39'),
(38, 1, 'Merlies Meoko, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:51:22', '2019-07-02 06:01:14'),
(39, 1, 'Pande Putu Lily Trisna Dewi, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:51:44', NULL),
(40, 1, 'Ruth Marina, Drg.', 'Universitas Sumatera Utara', NULL, NULL, NULL, '2019-07-02 05:52:00', '2019-09-12 04:29:22'),
(42, 1, 'Sariesendy, SP. Ortho, Drg.', 'Universitas Indonesia', NULL, NULL, NULL, '2019-07-02 05:52:24', '2019-09-12 04:27:55'),
(44, 1, 'Titus Dermawan, Drg.', 'Universitas Indonesia', NULL, NULL, NULL, '2019-07-02 05:52:39', '2019-09-12 04:28:06'),
(46, 1, 'Fifky Pantow, Drg.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:53:04', '2019-09-12 04:26:08'),
(48, 5, 'Kristin Indriati Hariningsih, SP, KK, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:53:27', '2019-07-02 05:57:55'),
(49, 5, 'Siskawati H.W. SP, KK, Dr.', 'Universitas', NULL, NULL, NULL, '2019-07-02 05:53:59', '2019-07-02 05:58:21'),
(51, 1, 'Medwin Setia, Drg.', NULL, NULL, NULL, NULL, '2019-07-02 05:54:15', '2019-10-02 04:55:02');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_locations`
--

CREATE TABLE `doctor_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_locations`
--

INSERT INTO `doctor_locations` (`id`, `doctor_id`, `location_id`, `created_at`, `updated_at`) VALUES
(10, 48, 5, '2019-07-02 05:57:00', NULL),
(11, 49, 5, '2019-07-02 05:59:02', NULL),
(12, 18, 1, '2019-07-02 05:59:15', NULL),
(13, 18, 5, '2019-07-02 05:59:26', NULL),
(14, 28, 5, '2019-07-02 05:59:38', NULL),
(15, 19, 5, '2019-07-02 05:59:49', NULL),
(16, 16, 1, '2019-07-02 06:00:02', NULL),
(17, 16, 5, '2019-07-02 06:00:14', NULL),
(18, 46, 5, '2019-07-02 06:00:24', NULL),
(19, 33, 1, '2019-07-02 06:00:33', NULL),
(20, 33, 5, '2019-07-02 06:00:42', NULL),
(21, 38, 5, '2019-07-02 06:01:37', NULL),
(22, 6, 1, '2019-07-02 06:01:52', NULL),
(23, 7, 1, '2019-07-02 06:01:59', NULL),
(24, 5, 1, '2019-07-10 05:48:15', NULL),
(25, 34, 1, '2019-07-10 05:48:29', NULL),
(26, 44, 1, '2019-07-10 05:48:38', NULL),
(27, 35, 1, '2019-07-10 05:49:44', NULL),
(28, 32, 1, '2019-07-10 05:49:56', NULL),
(29, 36, 1, '2019-07-10 05:50:03', NULL),
(30, 42, 1, '2019-07-10 05:50:10', NULL),
(31, 31, 1, '2019-07-10 05:50:19', NULL),
(32, 30, 1, '2019-07-10 05:50:27', NULL),
(33, 14, 1, '2019-07-10 05:50:41', NULL),
(34, 11, 1, '2019-07-10 05:50:55', NULL),
(35, 28, 1, '2019-07-10 05:51:40', NULL),
(36, 13, 1, '2019-07-10 05:52:04', NULL),
(37, 23, 1, '2019-07-10 05:52:14', NULL),
(38, 24, 1, '2019-07-10 05:52:28', NULL),
(39, 8, 1, '2019-07-10 05:52:37', NULL),
(40, 26, 1, '2019-07-10 05:52:45', NULL),
(41, 22, 1, '2019-07-10 05:53:07', NULL),
(42, 9, 1, '2019-07-10 05:53:16', NULL),
(43, 27, 1, '2019-07-10 05:53:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_specialists`
--

CREATE TABLE `doctor_specialists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_specialists`
--

INSERT INTO `doctor_specialists` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Gigi', '2019-06-05 22:45:21', NULL),
(2, 'Jantung', '2019-06-05 22:45:27', NULL),
(4, 'Umum', '2019-07-02 05:23:21', NULL),
(5, 'Kulit & Kelamin', '2019-07-02 05:23:29', NULL),
(6, 'Mata', '2019-07-02 05:23:34', NULL),
(7, 'Penyakit Dalam', '2019-07-02 05:23:41', NULL),
(8, 'Saraf', '2019-07-02 05:23:46', NULL),
(9, 'THT', '2019-07-02 05:23:56', NULL),
(10, 'Physicology', '2019-07-02 05:24:16', NULL),
(11, 'Kebidanan & Penyakit Kandungan', '2019-07-02 05:25:08', NULL),
(12, 'Anak', '2019-07-02 05:41:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pharmacy_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_pharmacy` tinyint(4) NOT NULL DEFAULT 0,
  `is_clinic` tinyint(4) NOT NULL DEFAULT 0,
  `clinic_schedule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pharmacy_schedule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `email`, `clinic_number`, `pharmacy_number`, `fax`, `whatsapp`, `address`, `created_at`, `updated_at`, `is_pharmacy`, `is_clinic`, `clinic_schedule`, `pharmacy_schedule`) VALUES
(1, 'Cilandak', NULL, '(021) 7883 6308', NULL, '(021) 7883 6307', NULL, 'Jl. Cilandak KKO Raya No. 45 Cilandak, Jakarta Selatan', '2019-06-05 22:55:02', '2019-07-08 07:25:38', 1, 1, '-', 'Senin - Jumat 07:00 - 22:00'),
(2, 'Cipete', NULL, '(021) 751 3850', '(021) 751 3850', '(021) 7590 6022', NULL, 'Cipete Raya No. 76 C Cipete, Jakarta Selatan', '2019-06-06 08:57:51', '2019-07-08 07:25:32', 1, 0, '-', 'Senin - Jumat 07:00 - 22:00'),
(3, 'Fatmawati', NULL, NULL, '(021) 751 9306', NULL, NULL, 'Jl. Fatmawati No. 46 C Fatmawati, Jakarta Selatan', '2019-06-06 08:58:52', '2019-07-08 07:25:05', 1, 0, '-', 'Senin - Jumat 07:00 - 22:00'),
(4, 'Cipedak', NULL, NULL, '(021) 788 83556', '(021) 788 83556', NULL, 'Jl. M.Kahfi I No. 45 Cipedak, Jakarta Selatan', '2019-06-06 08:59:43', '2019-07-08 07:25:23', 1, 0, '-', 'Setiap Hari 07:00 - 22:00'),
(5, 'Karang Tengah', NULL, '(021) 75909473', '(021) 7655433', NULL, NULL, 'Jl. Karang TengahRaya No.60 Lebak Bulus, Jakarta 12440', '2019-07-02 05:35:54', '2019-07-08 07:18:56', 1, 1, 'Senin - Jumat 07:00 - 22:00', 'Senin - Jumat 08:00 - 22:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(29, '2014_10_12_000000_create_users_table', 1),
(30, '2014_10_12_100000_create_password_resets_table', 1),
(31, '2016_08_07_145904_add_table_cms_apicustom', 1),
(32, '2016_08_07_150834_add_table_cms_dashboard', 1),
(33, '2016_08_07_151210_add_table_cms_logs', 1),
(34, '2016_08_07_151211_add_details_cms_logs', 1),
(35, '2016_08_07_152014_add_table_cms_privileges', 1),
(36, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(37, '2016_08_07_152320_add_table_cms_settings', 1),
(38, '2016_08_07_152421_add_table_cms_users', 1),
(39, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(40, '2016_08_07_154624_add_table_cms_moduls', 1),
(41, '2016_08_17_225409_add_status_cms_users', 1),
(42, '2016_08_20_125418_add_table_cms_notifications', 1),
(43, '2016_09_04_033706_add_table_cms_email_queues', 1),
(44, '2016_09_16_035347_add_group_setting', 1),
(45, '2016_09_16_045425_add_label_setting', 1),
(46, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(47, '2016_10_01_141740_add_method_type_apicustom', 1),
(48, '2016_10_01_141846_add_parameters_apicustom', 1),
(49, '2016_10_01_141934_add_responses_apicustom', 1),
(50, '2016_10_01_144826_add_table_apikey', 1),
(51, '2016_11_14_141657_create_cms_menus', 1),
(52, '2016_11_15_132350_create_cms_email_templates', 1),
(53, '2016_11_15_190410_create_cms_statistics', 1),
(54, '2016_11_17_102740_create_cms_statistic_components', 1),
(55, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(56, '2019_06_04_185548_create_services_table', 1),
(57, '2019_06_04_193111_create_visi_misis_table', 2),
(58, '2019_06_04_193820_create_abouts_table', 3),
(59, '2019_06_06_051213_create_doctors_table', 4),
(60, '2019_06_06_051437_create_doctor_specialists_table', 4),
(61, '2019_06_06_051532_create_doctor_locations_table', 4),
(62, '2019_06_06_051614_create_locations_table', 4),
(63, '2019_06_06_052033_create_schedule_headers_table', 4),
(64, '2019_06_06_052153_create_schedule_lines_table', 5),
(65, '2019_06_06_060631_alter_column_enum_to_string', 6),
(66, '2019_06_06_061228_add_schedule_header_to_schedule_lines', 7),
(67, '2019_06_06_062914_delete_schedule_header_id', 8),
(68, '2019_06_06_063154_delete_schedule_header_id', 9),
(69, '2019_06_06_181154_add_location_id_to_schedule_lines', 10),
(70, '2019_07_03_131000_add_more_column_to_locations', 11),
(71, '2019_07_08_140335_create_admin_whatsapp_numbers_table', 12),
(72, '2019_07_08_141221_add_column_to_locations', 13),
(73, '2019_07_14_190437_create_articles_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_lines`
--

CREATE TABLE `schedule_lines` (
  `id` int(10) UNSIGNED NOT NULL,
  `day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `schedule_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `doctor_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_lines`
--

INSERT INTO `schedule_lines` (`id`, `day`, `from_time`, `to_time`, `schedule_type`, `created_at`, `updated_at`, `doctor_id`, `location_id`) VALUES
(4, 'RABU', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:29:08', NULL, 48, 5),
(5, 'KAMIS', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:29:36', '2019-07-03 05:42:19', 48, 5),
(6, 'SENIN', '17:00:00', '17:30:00', 'Berdasarkan Kedatangan', '2019-07-03 02:30:29', NULL, 49, 5),
(7, 'SELASA', '17:00:00', '17:30:00', 'Berdasarkan Perjanjian', '2019-07-03 02:31:07', '2019-07-03 05:43:14', 49, 5),
(8, 'JUMAT', '17:00:00', '17:30:00', 'Berdasarkan Kedatangan', '2019-07-03 02:31:41', NULL, 49, 5),
(9, 'SABTU', '16:00:00', '16:30:00', 'Berdasarkan Kedatangan', '2019-07-03 02:32:11', NULL, 49, 5),
(10, 'SENIN', '09:00:00', '13:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:33:22', NULL, 18, 5),
(11, 'RABU', '12:59:00', '16:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:33:55', NULL, 18, 5),
(12, 'KAMIS', '09:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:34:44', NULL, 18, 5),
(13, 'KAMIS', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:35:21', NULL, 18, 5),
(14, 'JUMAT', '09:00:00', '15:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:36:19', '2019-07-10 05:54:10', 18, 5),
(15, 'SENIN', '13:00:00', '16:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:37:14', NULL, 28, 5),
(16, 'RABU', '09:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:38:02', NULL, 28, 5),
(17, 'SENIN', '17:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:38:33', NULL, 19, 5),
(18, 'SELASA', '09:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:39:08', NULL, 19, 5),
(19, 'RABU', '17:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:39:49', NULL, 19, 5),
(20, 'RABU', '17:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:39:49', NULL, 19, 5),
(21, 'KAMIS', '13:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:40:19', NULL, 19, 5),
(22, 'JUMAT', '17:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:41:18', NULL, 19, 5),
(23, 'SABTU', '09:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:41:58', NULL, 19, 5),
(24, 'SABTU', '17:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:43:11', NULL, 16, 5),
(25, 'SELASA', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-03 02:43:44', NULL, 46, 5),
(28, 'SELASA', '00:00:00', '00:00:00', 'Berdasarkan Perjanjian', '2019-07-10 05:55:58', NULL, 38, 5),
(29, 'KAMIS', '00:00:00', '00:00:00', 'Berdasarkan Perjanjian', '2019-07-10 05:56:16', NULL, 38, 5),
(30, 'SENIN', '15:00:00', '21:00:00', 'Berdasarkan Jam', '2019-07-10 05:57:54', NULL, 5, 1),
(31, 'RABU', '15:00:00', '21:00:00', 'Berdasarkan Jam', '2019-07-10 05:58:48', NULL, 5, 1),
(32, 'SELASA', '10:00:00', '14:00:00', 'Berdasarkan Jam', '2019-07-10 05:59:39', NULL, 5, 1),
(33, 'JUMAT', '10:00:00', '14:00:00', 'Berdasarkan Jam', '2019-07-10 06:00:13', NULL, 5, 1),
(34, 'KAMIS', '14:00:00', '18:00:00', 'Berdasarkan Jam', '2019-07-10 06:00:42', NULL, 5, 1),
(35, 'SABTU', '13:00:00', '17:00:00', 'Berdasarkan Jam', '2019-07-10 06:01:17', NULL, 5, 1),
(36, 'SENIN', '10:00:00', '13:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:14:26', NULL, 34, 1),
(37, 'RABU', '10:00:00', '13:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:15:24', NULL, 34, 1),
(38, 'KAMIS', '10:00:00', '13:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:15:57', NULL, 34, 1),
(39, 'SABTU', '10:00:00', '13:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:16:33', NULL, 34, 1),
(40, 'SELASA', '14:00:00', '18:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:17:06', NULL, 34, 1),
(41, 'RABU', '17:00:00', '19:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:17:33', NULL, 44, 1),
(42, 'JUMAT', '17:00:00', '19:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:18:03', NULL, 44, 1),
(43, 'SABTU', '17:00:00', '19:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:18:27', NULL, 34, 1),
(44, 'SELASA', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:19:06', NULL, 33, 1),
(45, 'KAMIS', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:19:31', NULL, 33, 1),
(46, 'SENIN', '15:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:20:04', NULL, 35, 1),
(47, 'KAMIS', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:20:35', NULL, 35, 1),
(48, 'KAMIS', '17:00:00', '18:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:21:08', NULL, 32, 1),
(49, 'SABTU', '00:00:00', '00:00:00', 'Berdasarkan Perjanjian', '2019-07-10 06:21:33', NULL, 36, 1),
(50, 'JUMAT', '00:00:00', '00:00:00', 'Berdasarkan Perjanjian', '2019-07-10 06:21:50', NULL, 42, 1),
(51, 'SENIN', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:22:23', NULL, 31, 1),
(52, 'SENIN', '10:00:00', '13:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:23:08', NULL, 30, 1),
(53, 'KAMIS', '19:00:00', '21:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:23:32', NULL, 30, 1),
(54, 'SENIN', '16:00:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:24:04', NULL, 14, 1),
(55, 'SELASA', '16:00:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:24:26', NULL, 14, 1),
(56, 'RABU', '16:00:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:24:53', NULL, 14, 1),
(57, 'KAMIS', '16:00:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:25:16', NULL, 14, 1),
(58, 'JUMAT', '19:00:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:25:49', NULL, 14, 1),
(59, 'SABTU', '19:00:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:26:11', NULL, 14, 1),
(60, 'SENIN', '19:30:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:27:04', NULL, 15, 1),
(61, 'SELASA', '19:30:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:27:25', NULL, 15, 1),
(62, 'RABU', '19:30:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:27:47', NULL, 15, 1),
(63, 'KAMIS', '19:30:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:28:10', NULL, 15, 1),
(64, 'JUMAT', '19:30:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:28:30', NULL, 15, 1),
(65, 'SABTU', '19:30:00', '22:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:28:54', NULL, 15, 1),
(66, 'SELASA', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:29:36', NULL, 11, 1),
(67, 'KAMIS', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:30:05', NULL, 11, 1),
(68, 'SENIN', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:30:48', NULL, 6, 1),
(69, 'SENIN', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:31:15', '2019-07-10 06:41:55', 6, 1),
(70, 'RABU', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:31:15', NULL, 6, 1),
(71, 'JUMAT', '14:00:00', '17:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:31:48', NULL, 6, 1),
(72, 'SABTU', '15:00:00', '18:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:42:31', NULL, 28, 1),
(73, 'SENIN', '08:30:00', '10:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:43:09', NULL, 7, 1),
(74, 'RABU', '08:30:00', '10:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:43:45', NULL, 7, 1),
(75, 'JUMAT', '08:30:00', '10:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:44:34', NULL, 7, 1),
(76, 'SELASA', '09:00:00', '15:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:45:32', NULL, 16, 1),
(77, 'KAMIS', '09:00:00', '15:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:46:14', NULL, 16, 1),
(78, 'SABTU', '09:00:00', '15:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:46:41', NULL, 16, 1),
(79, 'SENIN', '17:00:00', '18:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:47:19', NULL, 13, 1),
(80, 'RABU', '10:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:47:51', NULL, 23, 1),
(81, 'JUMAT', '10:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:53:58', NULL, 23, 1),
(82, 'SABTU', '10:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:54:40', NULL, 23, 1),
(83, 'SABTU', '10:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:54:40', NULL, 23, 1),
(84, 'SELASA', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:55:24', NULL, 23, 1),
(85, 'KAMIS', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:55:47', NULL, 23, 1),
(86, 'SENIN', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:56:20', NULL, 24, 1),
(87, 'RABU', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:56:52', NULL, 24, 1),
(88, 'JUMAT', '18:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:57:17', NULL, 24, 1),
(89, 'SENIN', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:57:54', NULL, 8, 1),
(90, 'JUMAT', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:58:28', NULL, 8, 1),
(91, 'SELASA', '20:00:00', '21:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:58:56', NULL, 8, 1),
(92, 'KAMIS', '20:00:00', '21:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:59:28', NULL, 8, 1),
(93, 'SELASA', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 06:59:58', NULL, 26, 1),
(94, 'KAMIS', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:00:28', NULL, 26, 1),
(95, 'SABTU', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:00:58', NULL, 26, 1),
(98, 'SENIN', '16:00:00', '18:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:02:23', NULL, 12, 1),
(99, 'JUMAT', '16:00:00', '18:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:02:49', NULL, 12, 1),
(100, 'SENIN', '18:30:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:03:25', NULL, 22, 1),
(101, 'SELASA', '18:30:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:03:56', NULL, 22, 1),
(102, 'KAMIS', '18:30:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:04:31', NULL, 22, 1),
(103, 'RABU', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:05:02', NULL, 9, 1),
(104, 'JUMAT', '19:00:00', '20:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:05:32', NULL, 9, 1),
(105, 'SABTU', '10:00:00', '12:00:00', 'Berdasarkan Kedatangan', '2019-07-10 07:06:09', NULL, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `slug`, `description`, `img`, `created_at`, `updated_at`) VALUES
(1, 'Apotek', 'apotek', '<p>Apotek Ajiwaras hanya menyediakan obat-obatan yang telah disetujui secara resmi dan telah menjalani kontrol yang sangat ketat untuk mengevaluasi kualitasnya sebelum disetujui. </p><p>Apotek kami sangat teliti dalam pembacaan resep maupun peracikan obat guna memberikan manfaat kesembuhan dan kesehatan bagi pasien\r\n\r\nGaransi obat-obatan yang berkualitas adalah salah satu bagian dalam pelayanan yang kami berikan kepada pasien. </p><p>Keramahan seluruh karyawan apotek dalam melayani pasien. Kejujuran serta kesahajaan dari pelayanan pelanggan sebagai suatu nilai tambah penting terhadap produk obat-obatan yang ditawarkan oleh apotek.\r\n</p><p>\r\nPembenahan secara terus-menerus dipastikan dilakukan pada seluruh cabang Apotek kami. </p><p>Realisasi dari proses perbaikan ditunjang oleh pengembangan desain terhadap produk baru, seperti penyediaan kelengkapan terhadap obat-obatan baik OTC, Etical dan obat-obatan tradisional Indonesia, fasilitas herbal medicine, serta penambahan keaneka ragaman produk yang dimiliki.\r\n</p><p>\r\nFilosofi mutu layanan Apotek :\r\n</p><p>1. Memberikan pelayanan terbaik dengan ketulusan hati untuk semua lapisan masyarakat</p><p>2. Kualitas obat yang terjamin secara mutu dengan harga terjangkau\r\n</p><p>3. Menjaga keaslian dan kelengkapan obat\r\n</p><p>4. Memberikan layanan pesan antar untuk daerah sekitar Apotek Ajiwaras</p>', 'uploads/1/2019-10/5.jpg', '2019-06-04 12:17:15', '2019-10-02 07:43:27'),
(2, 'Laboratorium', 'laboratorium', '<p>Laboratorium Ajiwaras dilengkapi dengan peralatan laboratorium yang lengkap dan tenaga analis yang profesional serta dibawah pengawasan yang ketat oleh dokter spesialis patologi klinik yang handal dan berpengalaman dibidangnya.\r\n</p><p>\r\nKami pastikan hasil laboratorium yang diberikan akurat dan tepat, sehingga dapat membantu dokter dalam memberikan diagnosa dan mengambil langkah yang tepat dalam penanganan, pengobatan dan pemulihan kesehatan anda.\r\n</p><p>\r\nFasilitas Pemeriksaan Laboratorium :\r\n</p><p>- Hematologi\r\nKoagulasi\r\nUrin\r\nTinja\r\nKimia Klinik\r\n</p><p>- Serologi\r\n</p><p>- Rematologi\r\nPetanda Tumor\r\n</p><p>- Tiroid\r\n</p><p>- Hormon Reproduksi\r\n</p><p>- Imunologi\r\nPetanda Tulang</p>', 'uploads/1/2019-10/0010376b_800.jpg', '2019-06-04 12:18:41', '2019-10-02 07:41:34'),
(3, 'Klinik', 'klinik', '<p>Klinik Ajiwaras merupakan bagian dari Ajiwaras Medical Center yang didirikan guna memberikan pelayanan kesehatan untuk masyarakat umum disekitarnya maupun perusahaan yang berada dalam jangkauan wilayahnya.</p><p><br></p><p>Klinik Ajiwaras dikelola dengan tim dokter spesialis dibidangnya masing-masing yang selalu siap untuk melayani pasien, dan dibantu oleh perawat ahli dan tenaga administrasi yang ramah.\r\n</p><p><br></p><p>Layanan yang diberikan, berkaitan dengan komitmen mutu dan berorientasi pada kepuasan pasien sehingga pasien dapat memperoleh jaminan kesehatan yang terbaik.\r\n</p><p><br></p><p>\r\nKlinik Ajiwaras Medical Center maju dan berkembang untuk memberikan layanan terbaik kepada masyarakat dengan mengikuti standar dan teknologi terkini. \r\n\r\nKlinik Ajiwaras disediakan bagi anda yang membutuhkan pelayanan terpercaya dan di tangani oleh dokter spesialis dibidangnya.\r\n</p><p>\r\nKlinik Spesialis Ajiwaras \r\nDokter Umum :\r\n</p><p>Dr. Gunawan D.J</p><p>Dr. H.M Afif Abbas\r\n</p><p>Dr. Asih Lubis\r\n</p><p>Dr. Hasyim Rusli</p><p>\r\nDr. Amiyati\r\n</p><p>Dr. Endang\r\n</p><p>Dr. Ismeila Murtie\r\n</p><p>Dr. Yety Retno L.\r\n</p><p>Dr. Kukuh Suryo</p><p>\r\nDr. Fifky Pantow</p><p>\r\n\r\nDokter Gigi :\r\n</p><p>Drg. Esther Juliawati</p><p>\r\nDrg. Ingrid Amelia\r\n</p><p>Drg. Titus Dermawan\r\n</p><p>Drg. Herna Emmy H\r\n</p><p>Drs. Lily, Sp. Perio</p><p>\r\nDrg. Hari Sumitro\r\n</p><p>Drg. Linda V. Sp. Ortho\r\n</p><p>Drg. Sariesendi, Sp. Ortho</p><p>\r\nDrg. Francis Monika Lago\r\n</p><p>Drg. Atria, Sp. KG\r\n<br>Drg. Merlies Meoko\r\n</p><p>\r\nDokter Spesialis Syaraf :\r\n</p><p>Dr. Fitriani Nasution, Sp.S\r\n</p><p>\r\nDokter Spesialis Anak :\r\n</p><p>Dr. Srie Enggar E, Sp. A\r\n</p><p>Dr. Sylvia Retnosari, Sp.A\r\n</p><p>\r\nDokter Spesialis Kandungan :\r\n</p><p>Dr. Dedy Soehermawan, Sp.OG\r\n</p><p>\r\nDokter Spesialis THT :</p><p>\r\nDr. TriDamiyatno, Sp. THT</p><p>\r\n\r\nDokter Spesialis Penyakit Dalam :</p><p> \r\nDr. Evy Novita, Sp. PD\r\n</p><p>\r\nDokter Spesialis Kulit dan Kelamin :\r\n</p><p>Dr. Rien N. Hadinoto, Sp. KK\r\n</p><p>Dr. Elly Moedijatmini, Sp. KK\r\n</p><p>Dr. Kristin Indrati Hariningsih, Sp. KK\r\n</p><p>Dr. Siskawati H.W, Sp. KK\r\n</p><p>\r\nDokter Spesialis Mata :\r\n</p><p>Dr. Indriani, Sp. M\r\n</p><p>Dr. Nusyirwan, Sp. M\r\n</p><p>Dr. Novita, Sp.M\r\n</p><p>\r\nPsikolog :</p><p>Drs. Yuli Susilowati, PSI</p>', 'uploads/1/2019-10/martha_dominguez_de_gouveia_nmym7fxpoke_unsplash.jpg', '2019-06-04 12:19:06', '2019-10-02 07:26:14'),
(4, 'Fisioterapi', 'fisioterapi', '<p><span style=\"color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif; font-size: 16px;\">Fisioterapi bertujuan untuk mengembalikan fungsi tubuh setelah terkena penyakit atau cedera. Jika tubuh menderita penyakit atau cedera permanen, maka fisioterapi dapat diprioritaskan untuk mengurangi dampaknya.</span><br></p><p>Fasilitas Layanan Fisioterapi : Elektroterapi Mekanoterapi Aktinoterapi Chest fisioterapi</p><div><br></div><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p><p style=\"font-size: 16px; margin-top: 10px; color: rgb(59, 55, 56); font-family: LatoWeb, sans-serif;\"><br></p>', 'uploads/1/2019-10/toa_heftiba_hblf2nvp_yc_unsplash.jpg', '2019-06-04 12:19:21', '2019-10-02 07:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visi_misi`
--

CREATE TABLE `visi_misi` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visi_misi`
--

INSERT INTO `visi_misi` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Misi', 'Memberikan pelayanan kesehatan yang prima, membangun manusia yang handal atau berkualitas dengan fasilitas dan teknologi yang tepat dan terbaharui.', '2019-06-04 12:33:40', '2019-06-04 12:34:06'),
(2, 'Visi', 'Menjadi institusi pelayanan kesehatan yang terbaik berdasarkan kasih sayang.', '2019-06-04 12:33:55', '2019-06-04 12:34:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_whatsapp_numbers`
--
ALTER TABLE `admin_whatsapp_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_locations`
--
ALTER TABLE `doctor_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_specialists`
--
ALTER TABLE `doctor_specialists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `schedule_lines`
--
ALTER TABLE `schedule_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visi_misi`
--
ALTER TABLE `visi_misi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_whatsapp_numbers`
--
ALTER TABLE `admin_whatsapp_numbers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `doctor_locations`
--
ALTER TABLE `doctor_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `doctor_specialists`
--
ALTER TABLE `doctor_specialists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `schedule_lines`
--
ALTER TABLE `schedule_lines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visi_misi`
--
ALTER TABLE `visi_misi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
