<nav id="ajw-navbar" class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                AJIWARAS
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('home.index') }}" onclick="singleClass.scrollToSetion('header');">Home</a></li>
                <li class="hd-md">
                    <a href="/home#service-section" onclick="singleClass.scrollToSetion('service-section');">Layanan <span class="fa fa-chevron-down fa-sm"></span></a>
                    <ul class="dropdown">
                        <li><a href="{{ route('service.index', 'apotek') }}">Apotek</a></li>
                        <li><a href="{{ route('service.index', 'laboratorium') }}">Laboratorium</a></li>
                        <li><a href="{{ route('service.index', 'klinik') }}">Klinik</a></li>
                        <li><a href="{{ route('service.index', 'fisioterapi') }}">Fisioterapi</a></li>
                    </ul>
                </li>
                <li class="d-md"><a href="{{ route('service.index', 'apotek') }}">Apotek</a></li>
                <li class="d-md"><a href="{{ route('service.index', 'laboratorium') }}">Laboratorium</a></li>
                <li class="d-md"><a href="{{ route('service.index', 'klinik') }}">Klinik</a></li>
                <li class="d-md"><a href="{{ route('service.index', 'fisioterapi') }}">Fisioterapi</a></li>
                <li><a href="/home#location-section" onclick="singleClass.scrollToSetion('location-section');">Lokasi & Jam Operasional</a></li>
                <li><a href="{{ route('schedule.index') }}">Jadwal Dokter</a></li>
                {{-- <li><a href="{{ route('article.index') }}">Artikel</a></li> --}}
            </ul>
        </div>
    </div>
</nav>