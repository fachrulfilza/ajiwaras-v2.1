<footer id="ajw-footer">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h2>Bergabung dengan kami</h2>
                <div class="row socmed-links">
                    <a href="#" class="fb"><span class="fab fa-facebook-f"></span></a>
                    <a href="#" class="tw"><span class="fab fa-twitter"></span></a>
                    <a href="#" class="ig"><span class="fab fa-instagram"></span></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                Copyright - Ajiwaras Medical Centre 2019
            </div>
            <div class="col-md-6">
                <a href="#">Back to Top <span class="fa fa-arrow-circle-up"></span></a>
            </div>
        </div>
    </div>
</footer>
@if($nomor_wa != '#')
    <a id="wa-btn" href="https://api.whatsapp.com/send?phone={{ $nomor_wa }}={{ $text_wa }}" class="float" target="_blank">
        <i class="fab fa-whatsapp my-float"></i>
    </a>
@else
    <a id="wa-btn" href="#" class="float disabled">
        <i class="fab fa-whatsapp my-float"></i>
    </a>
@endif
