@extends('layouts.app')

@section('content')
<div id="schedules-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="pre-loc">Pilih Lokasi Cabang</h2>
                <h2 id="title-section" class="end-loc"></h2>
            </div>
        </div>
        <div class="row">
            @foreach($locations as $location)
            <div class="pre-loc">
                <div class="card">
                    <div class="card-body">
                        <a href="javascript:;" onclick="scheduleClass.getScheduleByLoc({{ $location->id }}, '{{ $location->name }}')">
                            <div class="img-container">
                                <img src="{{ asset('images/placeholder.svg') }}" alt="location icon">
                            </div>
                        </a>
                        <a href="javascript:;" onclick="scheduleClass.getScheduleByLoc({{ $location->id }}, '{{ $location->name }}')">
                            <h2>{!! $location->name !!}</h2>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-md-6 text-center end-loc">
                <p>Cari Berdasarkan nama atau spesialis dokter</p>                
                <form action="">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
                        <input type="text" id="search-input" class="form-control" placeholder="Ketik nama atau spesialis dokter..." aria-describedby="sizing-addon1">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="loc-btn-container end-loc">
        <div class="row">
            <div class="col-md-12"></div>
        </div>
    </div>
    {{-- <div class="container pre-loc spes-container">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-spesialis"><span class="fa fa-search"></span> Cari Berdasarkan Spesialis</a>
            </div>
        </div>
    </div> --}}
    
    
    <div class="container-fluid end-loc">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body table-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
    <link href="{{ asset('css/schedule.css') }}" rel="stylesheet">
@endpush
@push('scripts')
    <script src="{{ asset('js/schedule.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#search-input').on('keyup change', function() {
                scheduleClass.getScheduleByq(`${$(this).val()}`);
            });
            $('form').submit(function(e){
                return false;
            });
        });
    </script>
@endpush