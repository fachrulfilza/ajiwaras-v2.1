@extends('layouts.app')

@section('content')
    <div class="articles-container">
        @foreach ($articles as $article)
            <div class="article-wrapper">
                <div class="article-header">
                    <div class="img-container">
                        <img src="{{ asset($article->image) }}" alt="Article Image">
                    </div>
                </div>
                <div class="article-body">
                    <div class="article-publish-at">
                        <span>{!! $article->created_at !!}</span>
                    </div>
                    <div class="article-text">
                        <h2>{!! ucwords($article->title) !!}</h2>
                        <p>{!! $article->description !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@push('styles')
    <link href="{{ asset('css/article.css') }}" rel="stylesheet">
@endpush