@extends('layouts.blog')
@section('content')
    <!-- Blog Classic Section -->
    <div class="container space-2 space-top-md-5 space-top-lg-4">
        @foreach ($articles as $article)
            @if($loop->iteration < count($articles))
                @if($loop->iteration%2 != 0)
                    <div class="card-deck d-block d-sm-flex card-sm-gutters-3 mb-sm-7">
                @endif
            @else
                @if(count($articles)%2 != 0)
                    <div class="card-deck d-block d-sm-flex card-sm-gutters-3 mb-sm-7">
                @endif
            @endif
                <!-- News Classic -->
                <article class="card border-0">
                    <div class="card-body p-0">
                        <div class="mb-5">
                        <img class="img-fluid w-100 rounded" src="{{ asset($article->image) }}" alt="Image Description">
                        </div>
                        <small class="d-block text-secondary mb-1">{!! $article->created_at !!}</small>
                        <h2 class="h5">
                        <a href="{{ route('article.show', $article->id) }}">{!! ucwords($article->title) !!}</a>
                        </h2>
                        <p>{!! str_limit($article->description, 100, ' ...') !!}</p>
                    </div>
                </article>
                <!-- End News Classic -->
            @if($loop->iteration < count($articles))
                @if($loop->iteration%2 == 0)
                </div>
                @endif
            @else
                @if(count($articles)%2 != 0)
                    </div>
                @endif
            @endif
        @endforeach

    </div>
    <!-- Blog Classic Section -->
@endsection

@push('styles')
    <link href="{{ asset('css/article.css') }}" rel="stylesheet">
@endpush