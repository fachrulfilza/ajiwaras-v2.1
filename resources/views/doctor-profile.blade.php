@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 card profile-header">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 text-center">
                        <div class="img-container">
                            <img src="{{ asset('images/user-circle-solid.svg') }}" alt="doctor profile img">
                        </div>
                        <h2>{!! ucwords($doctor->name) !!}</h2>
                        <div class="spes-container">
                            <h4>Spesialis</h4>
                            <span class="spes-label">{!! $doctor->specialist->name !!}</span>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="profile-desc-container">
                            <h4>Profil</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati accusantium, eaque laudantium alias consectetur vero, autem quaerat quo eum ut incidunt aliquam aspernatur eligendi ad, atque non porro adipisci placeat! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum, neque accusamus? Ratione, consequatur. Magni dolores quam, beatae officiis harum vel labore dolorum tenetur deleniti. Iusto, asperiores. Eligendi ex repellat sequi.</p>
                        </div>
                        <div class="profile-loc-container">
                            <h4>Lokasi Praktek</h4>
                            <ul>
                                @foreach($doctor->doctor_locations as $doctor_location)
                                    <li><span class="fas fa-map-pin"></span>{!! $doctor_location->location->name !!}</li>
                                @endforeach
                            </ul>
                            <a href="{{ route('schedule.index') }}" class="btn back"><span class="fa fa-chevron-left"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row schedule-container">
        <div class="col-md-12 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Jadwal Praktek</h2>
                    </div>
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Hari</th>
                            <th>Jam Praktek</th>
                            <th>Lokasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($doctor->schedule_lines as $schedule)
                        <tr>
                            <td>{!! ucwords(strtolower($schedule->day)) !!}</td>
                            <td>{!! str_limit($schedule->from_time, 5, '') !!} - {!! str_limit($schedule->to_time, 5, '') !!}</td>
                            <td>{!! $schedule->location->name !!}</td>
                        </tr>
                        @endforeach
                        @unless (count($doctor->schedule_lines) > 0)
                            <tr class="text-center">
                                <td colspan="3">Maaf jadwal tidak dapat ditemukan..</td>
                            </tr>    
                        @endunless
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
    <link href="{{ asset('css/doctor.css') }}" rel="stylesheet">
@endpush