@extends('layouts.app')

@section('content')
<header id="header">
    <h1>AJIWARAS MEDICAL CENTRE</h1>
    <div class="row h-content">
        @foreach ($visi_misi as $item)
        <div class="col-md-6">
            <h2>{!! $item->name !!}</h2>
            <p>{!! $item->description !!}</p>
        </div>
        @endforeach
    </div>
</header>
<div id="service-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Layanan</h2>
                <p>Berikut ini beberapa layanan di <b>Ajiwaras Medical Centre</b></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>Apotek</h3>
                        <div class="img-container">
                            <img src="{{ asset('images/flask.svg') }}" alt="logo">
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Kami hanya menyediakan obat-obatan yang telah disetujui secara resmi.</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('service.index', 'apotek') }}">Selengkapnya <span class="fas fa-chevron-right fa-sm"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>Laboratorium</h3>
                        <div class="img-container">
                            <img src="{{ asset('images/education.svg') }}" alt="logo">
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Peralatan laboratorium yang lengkap dan tenaga analisis profesional.</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('service.index', 'laboratorium') }}">Selengkapnya <span class="fas fa-chevron-right fa-sm"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>Klinik</h3>
                        <div class="img-container">
                            <img src="{{ asset('images/flask.svg') }}" alt="logo">
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Dikelola oleh tim dokter spesialis dibidangnya masing-masing.</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('service.index', 'klinik') }}">Selengkapnya <span class="fas fa-chevron-right fa-sm"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>Fisioterapi</h3>
                        <div class="img-container">
                            <img src="{{ asset('images/education.svg') }}" alt="logo">
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Fisioterapi bertujuan mengembalikan fungsi tubuh setelah terkena penyakit / cedera.</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('service.index', 'fisioterapi') }}">Selengkapnya <span class="fas fa-chevron-right fa-sm"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="img-container">
                    <img src="{{ asset($about->img ?: 'images/helloquence-61189-unsplash.jpg') }}" alt="about image">
                </div>
            </div>
            <div class="col-md-6">
                <h2>Tentang Kami</h2>
                <p>{!! str_limit($about->description, 370, ' ...') !!}</p>
                <a href="javascript:;" class="btn btn-read-more" data-toggle="modal" data-target=".modal-1">Baca Lebih Lanjut <span class="fa fa-chevron-right"></span></a>
                <div class="modal fade modal-1" tabindex="-1" role="dialog" aria-labelledby="modal-1" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content modal-content-ajw-desc">
                            {!! $about->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="location-section">
    <div class="row">
        <div class="d-lg col-md-12 text-center">
            <h2>Kantor Pusat & Cabang</h2>
            <p>Saat ini kami telah memiliki 1 kantor pusat & 4 cabang</p>
        </div>
        <div class="d-sm col-md-12 text-center">
            <h2>Lokasi Klinik & Apotek</h2>
        </div>
    </div>
    <div class="row">
        <div>
            <ul class="btn-container">
                @foreach ($locations as $location)
                <li id="btn-item-{{ $loop->iteration }}" class="btn-side-content-items {{ (strtolower($location->name) != 'cilandak' ?: 'active') }}">
                    <a href="javascript:;" onclick="singleClass.showSideContent('item-{{ $loop->iteration }}')">Ajiwaras Medical Centre {!! $location->name !!}</a>
                </li>
                @endforeach
            </ul>
        </div>
        <div>
            <div class="description">
                @foreach ($locations as $location)
                <div id="item-{!! $loop->iteration !!}" class="side-content-items">
                    <h3>{!! (strtolower($location->name) == 'cilandak' ? 'Kantor Pusat' : 'Cabang '.$location->name) !!}</h3>
                    <p>{!! $location->address !!}</p>
                    <div class="img-container">
                        <a href="https://www.google.com/maps/place/{{ $location->address }}" target="_blank">
                            <img src="{{ asset('images/placeholder.svg') }}" alt="location icon">
                        </a>
                    </div>
                    <ul>
                        @if($location->fax != null)
                        <li>
                            <span>Fax</span>
                            <span>{!! $location->fax !!}</span>
                        </li>
                        @endif
                        @if($location->clinic_number != null)
                        <li>
                            <span>Klinik</span>
                            <span>{!! $location->clinic_number !!}</span>
                        </li>
                        @endif
                        @if($location->pharmacy_number != null)
                        <li>
                            <span>Apotek</span>
                            <span>{!! $location->pharmacy_number !!}</span>
                        </li>
                        @endif
                        @if($location->pharmacy_schedule != null)
                        <li>
                            <span>Jadwal Apotek:</span>
                            <span>{!! $location->pharmacy_schedule !!}</span>
                        </li>
                        @endif
                        @if($location->clinic_schedule != null)
                        <li>
                            <span>Jadwal Klinik:</span>
                            <span>{!! $location->clinic_schedule !!}</span>
                        </li>
                        @endif
                    </ul>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
