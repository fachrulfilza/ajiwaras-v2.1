<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Title -->
  <title>Blog - Classic Full Width | Front - Responsive Website Template</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/fontawesome-all.min.css') }}">
  
  
  <!-- CSS Front Template -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/css/theme.css') }}">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    @include('partials._navbar')
    <!-- ========== MAIN ========== -->
    <main id="content" role="main">
        @yield('content')
    </main>
    <!-- ========== END MAIN ========== -->
    @include('partials._footer')

  <!-- JS Global Compulsory -->
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery-migrate/dist/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js') }}"></script>

  <!-- JS Front -->
  <script src="{{ asset('assets/js/hs.core.js') }}"></script>
  <script src="{{ asset('assets/js/components/hs.header.js') }}"></script>
  <script src="{{ asset('assets/js/components/hs.unfold.js') }}"></script>
  <script src="{{ asset('assets/js/components/hs.malihu-scrollbar.js') }}"></script>
  <script src="{{ asset('assets/js/components/hs.focus-state.js') }}"></script>
  <script src="{{ asset('assets/js/components/hs.show-animation.js') }}"></script>
  <script src="{{ asset('assets/js/components/hs.svg-injector.js') }}"></script>
</body>
</html>