@extends('layouts.app')

@section('content')
    <div id="service-page-section">
        <div class="row">
            <div class="col-md-12 has-bg flex-center" style="background-image: url('{{ asset($service->img) }}');">
                <div class="title">
                    <h2>{!! $service->name !!}</h2>
                </div>
                <div class="overlay"></div>
            </div>
            <div class="col-md-12">
                <div class="content">
                    <p>{!! $service->description !!}</p>
                </div>
            </div>
            <div class="col-md-12 flex-center">
                <a href="{{ route('home.index') }}" class="btn back-btn"><span class="fa fa-chevron-left"></span> Kembali</a>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link href="{{ asset('css/service.css') }}" rel="stylesheet">
@endpush