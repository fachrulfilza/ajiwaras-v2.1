<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/jadwal-dokter', 'ScheduleController@index')->name('schedule.index');
Route::get('/layanan/{slug}', 'ServiceController@index')->name('service.index');
Route::get('/dokter/{id}', 'DoctorController@show')->name('doctor.profile');
Route::get('/artikel', 'ArticleController@index')->name('article.index');
Route::get('/artikel/{id}', 'ArticleController@show')->name('article.show');

// ajax or json request
Route::get('/get/schedule/loc/{location}', 'ScheduleController@getScheduleByLoc');
Route::get('/get/locations', 'LocationController@getAllLocation');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
