var currentLoc = '';
var scheduleClass = function(){
    return{
        getScheduleByLoc: function(_loc, _name){
            //set current location
            currentLoc = _loc;
            //show loader in tbody row
            $(`.table-responsive`).html(`
                <div class="pre-load">
                    <div class="text-center">
                        <h4><span class="fa fa-spinner fa-spin"></span></h4>
                        <p>Mohon Tunggu..</p>
                    </div>
                </div>
            `);

            //reset input search value
            $('#search-input').val('');

            //fill the title with current location
            $('#title-section').html(`Jadwal Dokter Ajiwaras di ${_name}`);
            
            //switch the section
            $(`.pre-loc`).fadeOut('fast').promise().done(function(){
                $(`.end-loc`).fadeIn('fast');
            });

            //Request Get Location where not in q
            $.ajax({
                url: `/get/locations`,
                method: 'get',
                data: {
                    location_id: _loc
                }
            })
            .done(function(data){
                //clear the placeholder
                $('.loc-btn-container > .row > .col-md-12').html('');
                $.each( data, function( key, location ) {
                    $('.loc-btn-container > .row > .col-md-12').append(
                        `<a href="javascript:;" class="loc-btn" onclick="scheduleClass.getScheduleByLoc(${location.id}, '${location.name}')">${location.name}</a>`
                    );
                });
            })
            .fail(function(err){
                console.log(err);
            });
                
            //Request Get Schedule by Location
            $.ajax({
                url: `/get/schedule/loc/${_loc}`,
                method: 'get',
            })
            .done(function(data){
                scheduleClass.drawScheduleTable(data);
            })
            .fail(function(err){
                Swal.fire(
                    'Ooppss!',
                    'Terjadi kesalahan, coba beberapa saat lagi..',
                    'error'
                )
                console.log(err);
            });
        },
        getScheduleByq: function( _q){
            //show loader in tbody row
            $(`.table-responsive`).html(`
                <div class="pre-load">
                    <div class="text-center">
                        <h4><span class="fa fa-spinner fa-spin"></span></h4>
                        <p>Mohon Tunggu..</p>
                    </div>
                </div>
            `);
            $.ajax({
                url: `/get/schedule/loc/${currentLoc}`,
                method: 'get',
                data: {
                    q: _q
                }
            })
            .done(function(data){
                console.log(data);
                scheduleClass.drawScheduleTable(data);
            })
            .fail(function(err){
                Swal.fire(
                    'Ooppss!',
                    'Terjadi kesalahan, coba beberapa saat lagi..',
                    'error'
                )
                console.log(err);
            });
            return false;
        },
        drawScheduleTable: function(_data){
            var id = 0;
            var data = _data;
            
            //reset pagination
            $(`.pagination-container`).find('.btn-toolbar').fadeOut('fast');

            if(data.length > 0){
                //clear the placeholder
                $('.pre-load').hide('fast');
                $('.table-responsive').html('');
                $('.table-responsive').html(`
                    <table id="schedules" class="table ajw-table-list">
                    <thead>
                        <tr>
                            <th>Dokter</th>
                            <th>Spesialis</th>
                            <th>Senin</th>
                            <th>Selasa</th>
                            <th>Rabu</th>
                            <th>Kamis</th>
                            <th>Jumat</th>
                            <th>Sabtu</th>
                            <th>Minggu</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-12 pagination-container"></div>
                </div>
                `);
                //call method for draw table
                $.each( data, function( key, row ) {
                    $('#schedules > tbody').append($(
                        `<tr id="${id}">
                            <td class="ajw-dl">
                                <a href="/dokter/${row.doctor.id}">${row.doctor.name}</a>
                            </td>
                            <td>${row.doctor.specialist.name}</td>
                            <td class="SENIN"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td class="SELASA"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td class="RABU"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td class="KAMIS"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td class="JUMAT"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td class="SABTU"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td class="MINGGU"><span class="badge badge-default">Jadwal Kosong</span></td>
                            <td>
                                <a href="/dokter/${row.doctor.id}" class="btn btn-outline-primary btn-rounded" data-toggle="tooltip" title="Lihat Detail"><span class="fa fa-search"></span></a>
                            </td>
                        </tr>`
                    ).hide().fadeIn('fast')
                    );
                    $.each( row.doctor.schedule_lines, function( i, schedule ) {
                        let schedule_time = schedule.schedule_type == 'Berdasarkan Perjanjian' ? '<span class="badge badge-success">Berdasarkan Perjanjian</span>' : `${schedule.from_time.substr(0,5)} - ${schedule.to_time.substr(0,5)}`;
                        if(schedule.location_id == row.location.id){
                            if(schedule.day == 'SENIN'){
                                $(`#schedules > tbody > tr#${id} > td.SENIN`).html(schedule_time);
                            }else if(schedule.day == 'SELASA'){
                                $(`#schedules > tbody > tr#${id} > td.SELASA`).html(schedule_time);
                            }else if(schedule.day == 'RABU'){
                                $(`#schedules > tbody > tr#${id} > td.RABU`).html(schedule_time);
                            }else if(schedule.day == 'KAMIS'){
                                $(`#schedules > tbody > tr#${id} > td.KAMIS`).html(schedule_time);
                            }else if(schedule.day == 'JUMAT'){
                                $(`#schedules > tbody > tr#${id} > td.JUMAT`).html(schedule_time);
                            }else if(schedule.day == 'SABTU'){
                                $(`#schedules > tbody > tr#${id} > td.SABTU`).html(schedule_time);
                            }else if(schedule.day == 'MINGGU'){
                                $(`#schedules > tbody > tr#${id} > td.MINGGU`).html(schedule_time);
                            }
                        }
                    });
                    id++;
                });
                //add pagination buttons
                // if(data.length > 8){
                //     $(`.pagination-container`).append($(`
                //     <div class="btn-toolbar" role="toolbar">
                //         <div class="btn-group" role="group"><a href="#" class="btn-pag"><span class="fa fa-chevron-left fa-sm"></span></a></div>
                //         <div class="btn-group" role="group"><a href="#" class="btn-pag">1</a></div>
                //         <div class="btn-group" role="group"><a href="#" class="btn-pag"><span class="fa fa-chevron-right fa-sm"></span></a></div>
                //     </div>`
                //     ).hide().fadeIn('fast'));
                // }
            }else{
                $(`.pre-load`).fadeOut('fast').promise().done(function(){
                    $(`.pre-load h4`).html(`<span class="fa fa-smile"></span>`);
                    $(`.pre-load p`).html(`Maaf, jadwal tidak dapat ditemukan..`);
                    $(`.pre-load`).fadeIn('fast');
                });
            }
        }
    }
}();