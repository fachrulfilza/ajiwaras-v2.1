var singleClass = function(){
    return{
        showSideContent: function(_id, _class = 'side-content-items'){
            $(`.btn-${_class}`).removeClass('active');
            $(`#btn-${_id}`).addClass('active');
            $(`.${_class}`).fadeOut('fast').promise().done(function(){
                $(`#${_id}`).fadeIn('fast');
            });
        
            return false;
        },
        scrollToSetion: function(_id){
            $('html, body').animate({
                scrollTop: $(`#${_id}`).offset().top
            }, 1000);
        },
    }
}();